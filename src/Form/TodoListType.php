<?php

namespace App\Form;

use App\Entity\TagTodo;
use App\Entity\TodoList;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TodoListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle')
            ->add('description', CKEditorType::class)
            ->add('priority',  ChoiceType::class, [
                'choices' => [
                    'Trés Haute Priorité' => 1,
                    'Haute Priorité' => 2,
                    'Moyenne Priorité' => 3,
                    'Basse Priorité' => 4,
                    'Négligeable' => 5,
                ],
            ])
            ->add('tags', EntityType::class, array(
                'class' => TagTodo::class,
                'choice_label' => 'libelle',
                'multiple' => true
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TodoList::class,
        ]);
    }
}
