<?php

namespace App\Form;

use App\Entity\Ressource;
use App\Entity\VilleRessources;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VilleRessourcesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantite')
            ->add('fertilite')
            /*->add('ville')*/
            ->add('ressource', EntityType::class, [
                'class' => Ressource::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')->where('r.parents is empty');
                }
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VilleRessources::class,
        ]);
    }
}
