<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201115124749 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE tag_todo (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, couleurs VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE todo_list (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, priority INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE todo_list_tag_todo (todo_list_id INT NOT NULL, tag_todo_id INT NOT NULL, INDEX IDX_5FFDAC39E8A7DCFA (todo_list_id), INDEX IDX_5FFDAC39D82F9199 (tag_todo_id), PRIMARY KEY(todo_list_id, tag_todo_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE todo_list_tag_todo ADD CONSTRAINT FK_5FFDAC39E8A7DCFA FOREIGN KEY (todo_list_id) REFERENCES todo_list (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE todo_list_tag_todo ADD CONSTRAINT FK_5FFDAC39D82F9199 FOREIGN KEY (tag_todo_id) REFERENCES tag_todo (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE migration_versions');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE todo_list_tag_todo DROP FOREIGN KEY FK_5FFDAC39D82F9199');
        $this->addSql('ALTER TABLE todo_list_tag_todo DROP FOREIGN KEY FK_5FFDAC39E8A7DCFA');
        $this->addSql('CREATE TABLE migration_versions (version VARCHAR(14) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, executed_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(version)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE tag_todo');
        $this->addSql('DROP TABLE todo_list');
        $this->addSql('DROP TABLE todo_list_tag_todo');
    }
}
