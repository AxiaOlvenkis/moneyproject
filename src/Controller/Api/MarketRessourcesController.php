<?php

// src/Controller/HelloController.php
namespace App\Controller\Api;

use App\Entity\MarketRessources;
use App\Entity\Ressource;
use App\Entity\Ville;
use App\Entity\VilleRessources;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MarketRessourcesController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @return VilleRessources[]|object[]
     */
    public function __invoke(Request $request)
    {
        $idR = $request->get('ressource');
        $idV = $request->get('ville');
        $critere = array();

        if($idR != null):
            $ressource = $this->em->getRepository(Ressource::class)->find($idR);
            $critere['ressource'] = $ressource;
        endif;

        if($idV != null):
            $ville = $this->em->getRepository(Ville::class)->find($idV);
            $critere['ville'] = $ville;
        endif;

        return $this->em->getRepository(MarketRessources::class)->findBy($critere);
    }
}