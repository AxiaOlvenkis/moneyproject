<?php

// src/Controller/HelloController.php
namespace App\Controller\Api;

use App\Entity\Page;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class PageController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param $slug
     * @return Page
     */
    public function __invoke(Request $request, $slug): ?Page
    {
        $page = $this->em->getRepository(Page::class)->findOneBy(array('slug' => $slug));

        return $page;
    }
}