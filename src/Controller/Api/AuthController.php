<?php

// src/Controller/HelloController.php
namespace App\Controller\Api;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var UserPasswordEncoderInterface */
    private $encoder;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $em;
        $this->encoder = $encoder;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request): ?JsonResponse
    {
        if($content = $request->getContent()){
            $reponse = json_decode($content);

            $email = $reponse->email;
            $username = $reponse->username;
            $password = $reponse->password;

            $user = new User();
            $user->setEmail($email);
            $user->setFullName($username);
            $user->setUsername(strtolower($username));
            $user->setPlainPassword($password);
            $user->setRoles(array('ROLE_USER'));
            $user->setPassword($this->encoder->encodePassword($user, $password));

            try {
                $this->em->persist($user);
                $this->em->flush();

                $code   = Response::HTTP_OK;
                $retour = sprintf('User %s successfully created', $user->getUsername());
            }
            catch(DBALException $e){
                $code = Response::HTTP_INTERNAL_SERVER_ERROR;
                $retour = $e->getMessage();
            }
            catch(\Exception $e){
                $code = Response::HTTP_INTERNAL_SERVER_ERROR;
                $retour = $e->getMessage();
            }
        }else{
            $code = 500;
            $retour = "no data";
        }

        $json = new JsonResponse();
        $json->setStatusCode($code);
        $json->setData(array('data' => $retour));
        return $json;
    }
}