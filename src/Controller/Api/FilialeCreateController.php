<?php

// src/Controller/HelloController.php
namespace App\Controller\Api;

use App\Entity\Page;
use App\Entity\SocieteFiliales;
use App\Entity\Societes;
use App\Entity\TypesEntreprises;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\VilleRessources;
use App\Service\SocietesServices;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FilialeCreateController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var SocietesServices $societeServices */
    private $societeServices;

    /**
     * @param EntityManagerInterface $em
     * @param SocietesServices $societeServices
     */
    public function __construct(EntityManagerInterface $em, SocietesServices $societeServices)
    {
        $this->em = $em;
        $this->societeServices = $societeServices;
    }

    /**
     * @param Request $request
     * @return Societes|null
     * @throws NonUniqueResultException
     */
    public function __invoke(Request $request): ?Societes
    {
        $n_societe = $request->get('societe');
        $n_filiale = $request->get('filiale');
        $id_user = $request->get('user');
        $id_ville = $request->get('ville');
        $id_type = $request->get('type');
        $id_ress = $request->get('ressource');

        $user = $this->em->getRepository(User::class)->find($id_user);
        $ville = $this->em->getRepository(Ville::class)->find($id_ville);
        $type = $this->em->getRepository(TypesEntreprises::class)->find($id_type);

        if($id_ress != 0){
            $ressource = $this->em->getRepository(VilleRessources::class)->find($id_ress);
        }else{
            $ressource = null;
        }

        $societe = $this->em->getRepository(Societes::class)->find($n_societe);
        $societe->setUser($user);
        $societe = $this->societeServices->create_filiale($societe, $n_filiale, $type, $ville, $ressource);

        $this->societeServices->execUpdate($societe);

        return $this->em->getRepository(Societes::class)->findOneBy(array('user' => $user));
    }
}