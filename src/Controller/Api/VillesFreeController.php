<?php

// src/Controller/HelloController.php
namespace App\Controller\Api;

use App\Entity\Ville;
use App\Entity\VilleRessources;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class VillesFreeController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @return VilleRessources[]|object[]
     */
    public function __invoke(Request $request)
    {
        $ids = $request->get('ids');

        $villes = $this->em->getRepository(Ville::class)->findFreeRessources($ids);
        return $villes;
    }
}