<?php

// src/Controller/HelloController.php
namespace App\Controller\Admin;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\UserServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/utilisateurs")
 */
class UserController extends AbstractController
{
    /** @var UserServices */
    private $userServices;

    /**
     * AddMapHistoryController constructor.
     * @param UserServices $userServices
     */
    public function __construct(UserServices $userServices)
    {
        $this->userServices = $userServices;
    }

    /**
     * @Route("/admin", name="lstadmin_admin", methods={"GET"})
     */
    public function admin(UserRepository $uRepo)
    {
        $users = $uRepo->findByRoles('ROLE_ADMIN');

        return $this->render('admin/users/user.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/", name="user_admin", methods={"GET"})
     */
    public function user(UserRepository $uRepo)
    {
        $users = $uRepo->findAll();

        return $this->render('admin/users/user.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/delete/{id}", name="user_delete", methods={"POST"})
     */
    public function delete(Request $request, User $user): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('user_admin');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $this->addFlash('success', 'post.deleted_successfully');

        return $this->redirectToRoute('user_admin');
    }

    /**
     * @Route("/edit/{id<\d+>}", name="user_edit")
     */
    public function edit(Request $request, User $user): Response
    {
        if ($request->isMethod('post')) {
            $this->userServices->updateUser($user, $request->request->all());
            return $this->redirectToRoute('user_admin');
        }

        return $this->render('admin/users/edit.html.twig', [
            'user' => $user
        ]);
    }
}