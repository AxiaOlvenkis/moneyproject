<?php

// src/Controller/HelloController.php
namespace App\Controller\Admin;

use App\Entity\Pays;
use App\Entity\Region;
use App\Form\RegionType;
use App\Repository\RegionRepository;
use App\Service\RegionServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/region")
 */
class RegionController extends AbstractController
{
    /** @var RegionServices */
    private $regionServices;

    /**
     * AddMapHistoryController constructor.
     * @param RegionServices $regionServices
     */
    public function __construct(RegionServices $regionServices)
    {
        $this->regionServices = $regionServices;
    }

    /**
     * @Route("/{id}", name="region_admin", methods={"GET"})
     */
    public function liste(RegionRepository $pRepo, Pays $pays)
    {
        $regions = $pRepo->findBy(array('pays' => $pays));

        return $this->render('admin/gestionpays/region/liste.html.twig', ['regions' => $regions, 'pays' => $pays]);
    }

    /**
     * @Route("/delete/{id}", name="region_delete", methods={"POST"})
     */
    public function delete(Request $request, Region $region): Response
    {
        $redirectId = $region->getPays()->getId();


        if ($this->isCsrfTokenValid('delete'.$region->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($region);
            $entityManager->flush();
        }

        return $this->redirectToRoute('region_admin', array('id' => $redirectId));
    }

    /**
     * @Route("/edit/{id<\d+>}", name="region_edit")
     */
    public function edit(Request $request, Region $region): Response
    {
        $form = $this->createForm(RegionType::class, $region);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('region_admin', array('id' => $region->getPays()->getId()));
        }

        return $this->render('admin/gestionpays/region/edit.html.twig', [
            'region' => $region,
            'pays' => $region->getPays(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/add/{id<\d+>}", name="region_add")
     */
    public function add(Request $request, Pays $pays): Response
    {
        $region = new Region();
        $form = $this->createForm(RegionType::class, $region);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $region->setPays($pays);
            $this->regionServices->execUpdate($region);
            return $this->redirectToRoute('region_admin', array('id' => $pays->getId()));
        }

        return $this->render('admin/gestionpays/region/add.html.twig', [
            'region' => $region,
            'pays' => $pays,
            'form' => $form->createView(),
        ]);
    }
}