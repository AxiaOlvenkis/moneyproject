<?php

namespace App\Controller\Admin;

use App\Entity\Changelog;
use App\Form\ChangelogType;
use App\Repository\ChangelogRepository;
use App\Service\ChangelogServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/changelog")
 */
class ChangelogController extends AbstractController
{
    /** @var ChangelogServices */
    private $changelogServices;

    /**
     * AddMapHistoryController constructor.
     * @param ChangelogServices $changelogServices
     */
    public function __construct(ChangelogServices $changelogServices)
    {
        $this->changelogServices = $changelogServices;
    }

    /**
     * @Route("/", name="changelog_index", methods={"GET"})
     */
    public function index(ChangelogRepository $changelogRepository): Response
    {
        return $this->render('admin/changelog/index.html.twig', [
            'changelogs' => $changelogRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="changelog_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $changelog = new Changelog();
        $form = $this->createForm(ChangelogType::class, $changelog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->changelogServices->execUpdate($changelog);
            return $this->redirectToRoute('changelog_index');
        }

        return $this->render('admin/changelog/new.html.twig', [
            'changelog' => $changelog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="changelog_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Changelog $changelog): Response
    {
        $form = $this->createForm(ChangelogType::class, $changelog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('changelog_index');
        }

        return $this->render('admin/changelog/edit.html.twig', [
            'changelog' => $changelog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="changelog_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Changelog $changelog): Response
    {
        if ($this->isCsrfTokenValid('delete'.$changelog->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($changelog);
            $entityManager->flush();
        }

        return $this->redirectToRoute('changelog_index');
    }
}
