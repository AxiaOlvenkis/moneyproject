<?php

// src/Controller/HelloController.php
namespace App\Controller\Admin;

use App\Entity\Ressource;
use App\Form\RessourceType;
use App\Repository\RessourceRepository;
use App\Service\RessourceServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/ressource")
 */
class RessourcesController extends AbstractController
{
    /** @var RessourceServices */
    private $ressourceServices;

    /**
     * AddMapHistoryController constructor.
     * @param RessourceServices $ressourceServices
     */
    public function __construct(RessourceServices $ressourceServices)
    {
        $this->ressourceServices = $ressourceServices;
    }

    /**
     * @Route("/", name="ressource_admin", methods={"GET"})
     */
    public function liste(RessourceRepository $pRepo)
    {
        $ressources = $pRepo->findAll();

        return $this->render('admin/gestion/ressources/liste.html.twig', ['ressources' => $ressources]);
    }

    /**
     * @Route("/delete/{id}", name="ressource_delete", methods={"POST"})
     */
    public function delete(Request $request, Ressource $ressource): Response
    {
        if (!$this->isCsrfTokenValid('delete'.$ressource->getId(), $request->request->get('token'))) {
            return $this->redirectToRoute('ressource_admin');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($ressource);
        $em->flush();

        $this->addFlash('success', 'post.deleted_successfully');

        return $this->redirectToRoute('ressource_admin');
    }

    /**
     * @Route("/edit/{id<\d+>}", name="ressource_edit")
     */
    public function edit(Request $request, RessourceRepository $pRepo, Ressource $ressource): Response
    {
        $form = $this->createForm(RessourceType::class, $ressource);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('ressource_admin');
        }

        $parents = $pRepo->findAll();
        return $this->render('admin/gestion/ressources/edit.html.twig', [
            'parents' => $parents,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/add", name="ressource_add")
     */
    public function add(Request $request, RessourceRepository $pRepo): Response
    {
        $ressource = new Ressource();
        $form = $this->createForm(RessourceType::class, $ressource);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->ressourceServices->execUpdate($ressource);
            return $this->redirectToRoute('ressource_admin');
        }

        $parents = $pRepo->findAll();
        return $this->render('admin/gestion/ressources/add.html.twig', [
            'parents' => $parents,
            'form' => $form->createView(),
        ]);
    }
}