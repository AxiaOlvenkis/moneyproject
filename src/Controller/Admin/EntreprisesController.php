<?php

// src/Controller/HelloController.php
namespace App\Controller\Admin;

use App\Repository\SocietesRepository;
use App\Service\SocietesServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/entreprises")
 */
class EntreprisesController extends AbstractController
{
    /** @var SocietesRepository $societeRepository*/
    private $societeServices;

    /**
     * AddMapHistoryController constructor.
     * @param SocietesServices $societeServices
     */
    public function __construct(SocietesServices $societeServices)
    {
        $this->societeServices = $societeServices;
    }

    /**
     * @Route("/", name="entreprises_admin", methods={"GET"})
     */
    public function liste(SocietesRepository $sRepo)
    {
        $liste = $sRepo->findAll();
        return $this->render('admin/gestion/entreprises/liste.html.twig', ['liste' => $liste]);
    }

    /**
     * @Route("/generate", name="entreprises_generate", methods={"GET"})
     */
    public function generate(SocietesRepository $sRepo)
    {
        $this->societeServices->delete_ia_entreprise();
        $this->societeServices->generate_ia_entreprises();

        //on met les sociétés IA en vente
        $this->societeServices->sendIAtoMarket();

        return $this->redirectToRoute('entreprises_admin');
    }
}