<?php

namespace App\Controller\Admin;

use App\Entity\TagTodo;
use App\Form\TagTodoType;
use App\Repository\TagTodoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/tagtodo")
 */
class TagTodoController extends AbstractController
{
    /**
     * @Route("/", name="tag_todo_index", methods={"GET"})
     */
    public function index(TagTodoRepository $tagTodoRepository): Response
    {
        return $this->render('admin/tag_todo/index.html.twig', [
            'tag_todos' => $tagTodoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tag_todo_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $tagTodo = new TagTodo();
        $form = $this->createForm(TagTodoType::class, $tagTodo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tagTodo);
            $entityManager->flush();

            return $this->redirectToRoute('tag_todo_index');
        }

        return $this->render('admin/tag_todo/new.html.twig', [
            'tag_todo' => $tagTodo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tag_todo_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TagTodo $tagTodo): Response
    {
        $form = $this->createForm(TagTodoType::class, $tagTodo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tag_todo_index');
        }

        return $this->render('admin/tag_todo/edit.html.twig', [
            'tag_todo' => $tagTodo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tag_todo_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TagTodo $tagTodo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tagTodo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tagTodo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tag_todo_index');
    }
}
