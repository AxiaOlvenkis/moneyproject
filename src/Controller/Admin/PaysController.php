<?php

// src/Controller/HelloController.php
namespace App\Controller\Admin;

use App\Entity\Pays;
use App\Form\PaysType;
use App\Repository\PaysRepository;
use App\Service\PaysServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/pays")
 */
class PaysController extends AbstractController
{
    /** @var PaysServices */
    private $paysServices;

    /**
     * AddMapHistoryController constructor.
     * @param PaysServices $paysServices
     */
    public function __construct(PaysServices $paysServices)
    {
        $this->paysServices = $paysServices;
    }

    /**
     * @Route("/", name="pays_admin", methods={"GET"})
     */
    public function liste(PaysRepository $pRepo)
    {
        $pays = $pRepo->findAll();

        return $this->render('admin/gestionpays/pays/liste.html.twig', ['pays' => $pays]);
    }

    /**
     * @Route("/delete/{id}", name="pays_delete", methods={"POST"})
     */
    public function delete(Request $request, Pays $pays): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pays->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pays);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pays_admin');
    }

    /**
     * @Route("/edit/{id<\d+>}", name="pays_edit")
     */
    public function edit(Request $request, Pays $pays): Response
    {
        $form = $this->createForm(PaysType::class, $pays);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pays_admin');
        }

        return $this->render('admin/gestionpays/pays/edit.html.twig', [
            'pay' => $pays,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/add", name="pays_add")
     */
    public function add(Request $request): Response
    {
        $pays = new Pays();
        $form = $this->createForm(PaysType::class, $pays);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->paysServices->execUpdate($pays);

            return $this->redirectToRoute('pays_admin');
        }

        return $this->render('admin/gestionpays/pays/add.html.twig', [
            'pays' => $pays,
            'form' => $form->createView(),
        ]);
    }
}