<?php

// src/Controller/HelloController.php
namespace App\Controller\Admin;

use App\Entity\TemporaryRecap;
use App\Entity\TemporaryVilles;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/tour")
 */
class TourController extends AbstractController
{
    /**
     * @Route("/recap", name="tour_recap", methods={"GET"})
     */
    public function recap()
    {
        $em = $this->getDoctrine()->getManager();
        $liste = $em->getRepository(TemporaryRecap::class)->findAll();
        return $this->render('admin/recap/recap.html.twig', ['liste' => $liste]);
    }

    /**
     * @Route("/recap/ville", name="tour_recap_ville", methods={"GET"})
     */
    public function recap_ville()
    {
        $em = $this->getDoctrine()->getManager();
        $liste = $em->getRepository(TemporaryVilles::class)->findAll();
        return $this->render('admin/recap/ville.html.twig', ['liste' => $liste]);
    }
}