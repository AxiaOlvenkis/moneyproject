<?php

// src/Controller/HelloController.php
namespace App\Controller\Admin;

use App\Entity\Pays;
use App\Entity\Region;
use App\Entity\TemporaryRecap;
use App\Entity\TemporaryVilles;
use App\Entity\Ville;
use App\Entity\VilleRessources;
use App\Repository\PaysRepository;
use App\Repository\RegionRepository;
use App\Repository\RessourceRepository;
use App\Repository\VilleRepository;
use App\Service\GameServices;
use App\Service\ProductionServices;
use App\Service\SocietesServices;
use App\Service\VilleServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class MainController extends AbstractController
{
    /** @var VilleServices */
    private $villeServices;

    /** @var SocietesServices */
    private $societeServices;

    /** @var ProductionServices */
    private $prodServices;

    /**
     * AddMapHistoryController constructor.
     * @param VilleServices $villeServices
     * @param SocietesServices $societeServices
     */
    public function __construct(VilleServices $villeServices, SocietesServices $societeServices, ProductionServices $prodServices)
    {
        $this->villeServices = $villeServices;
        $this->societeServices = $societeServices;
        $this->prodServices = $prodServices;
    }

    /**
     * @Route("/", name="home_admin", methods={"GET"})
     */
    public function home()
    {
        return $this->render('admin/index.html.twig');
    }

    /**
     * @Route("/new/day", name="generate_new_day", methods={"GET"})
     */
    public function newday(){
        $em = $this->getDoctrine()->getManager();

        /** 1- On genere les gains de la ville pour ce tour */
        $recapV = $em->getRepository(TemporaryVilles::class)->findAll();
        foreach ($recapV as $k => $rv):
            $this->villeServices->maj_city($rv);
            $em->remove($rv);
        endforeach;
        $villes = $em->getRepository(Ville::class)->findAll();
        foreach ($villes as $k => $v):
            $this->villeServices->dayCity($v);
        endforeach;

        /** 2- On bascule tout les gains du tour précédent en réel et on vide les listes*/
        $recap = $em->getRepository(TemporaryRecap::class)->findAll();
        foreach ($recap as $k => $r):
            $this->societeServices->maj_filiale($r);
            $em->remove($r);
        endforeach;
        $em->flush();

        /** 4- On genere toute les productions */
        $this->prodServices->generate();

        return $this->redirectToRoute('tour_recap');
    }
}