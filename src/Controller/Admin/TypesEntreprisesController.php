<?php

// src/Controller/HelloController.php
namespace App\Controller\Admin;

use App\Entity\TypesEntreprises;
use App\Entity\Ville;
use App\Entity\Region;
use App\Form\TypesEntreprisesType;
use App\Repository\TypesEntreprisesRepository;
use App\Repository\VilleRepository;
use App\Repository\RessourceRepository;
use App\Service\TypesEntreprisesServices;
use App\Service\VilleServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/typesentreprises")
 */
class TypesEntreprisesController extends AbstractController
{
    /** @var TypesEntreprisesServices */
    private $typesEntreprisesServices;

    /**
     * AddMapHistoryController constructor.
     * @param TypesEntreprisesServices $typesEntreprisesServices
     */
    public function __construct(TypesEntreprisesServices $typesEntreprisesServices)
    {
        $this->typesEntreprisesServices = $typesEntreprisesServices;
    }

    /**
     * @Route("/", name="types_entreprises_admin", methods={"GET"})
     */
    public function liste(TypesEntreprisesRepository $pRepo)
    {
        $types = $pRepo->findAll();

        return $this->render('admin/gestion/typesEntreprises/liste.html.twig', ['types' => $types]);
    }

    /**
     * @Route("/delete/{id}", name="types_entreprises_delete", methods={"POST"})
     */
    public function delete(Request $request, TypesEntreprises $types): Response
    {
        if (!$this->isCsrfTokenValid('delete'.$types->getId(), $request->request->get('token'))) {
            return $this->redirectToRoute('types_entreprises_admin');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($types);
        $em->flush();

        $this->addFlash('success', 'post.deleted_successfully');

        return $this->redirectToRoute('types_entreprises_admin');
    }

    /**
     * @Route("/edit/{id<\d+>}", name="types_entreprises_edit")
     */
    public function edit(Request $request, RessourceRepository $pRepo, TypesEntreprises $type): Response
    {
        $form = $this->createForm(TypesEntreprisesType::class, $type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('types_entreprises_admin');
        }

        $ressources = $pRepo->findAll();
        return $this->render('admin/gestion/typesEntreprises/edit.html.twig', [
            'ressources' => $ressources,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/add", name="types_entreprises_add")
     */
    public function add(Request $request, RessourceRepository $pRepo): Response
    {
        $type = new TypesEntreprises();
        $form = $this->createForm(TypesEntreprisesType::class, $type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->typesEntreprisesServices->execUpdate($type);

            return $this->redirectToRoute('types_entreprises_admin');
        }

        $ressources = $pRepo->findAll();
        return $this->render('admin/gestion/typesEntreprises/add.html.twig', [
            'ressources' => $ressources,
            'form' => $form->createView(),
        ]);
    }
}