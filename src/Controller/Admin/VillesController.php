<?php

// src/Controller/HelloController.php
namespace App\Controller\Admin;

use App\Entity\Ville;
use App\Entity\Region;
use App\Form\VilleRessourcesType;
use App\Form\VilleType;
use App\Repository\VilleRepository;
use App\Repository\RessourceRepository;
use App\Service\VilleServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/ville")
 */
class VillesController extends AbstractController
{
    /** @var VilleServices */
    private $villeServices;

    /**
     * AddMapHistoryController constructor.
     * @param VilleServices $villeServices
     */
    public function __construct(VilleServices $villeServices)
    {
        $this->villeServices = $villeServices;
    }

    /**
     * @Route("/{id}", name="ville_admin", methods={"GET"})
     */
    public function liste(VilleRepository $pRepo, Region $region)
    {
        $villes = $pRepo->findBy(array('region' => $region));

        return $this->render('admin/gestionpays/ville/liste.html.twig', ['villes' => $villes, 'region' => $region]);
    }

    /**
     * @Route("/delete/{id}", name="ville_delete", methods={"POST"})
     */
    public function delete(Request $request, Ville $ville): Response
    {
        $redirectId = $ville->getRegion()->getId();
        if ($this->isCsrfTokenValid('delete'.$ville->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ville);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ville_admin', array('id' => $redirectId));
    }

    /**
     * @Route("/edit/{id<\d+>}", name="ville_edit")
     */
    public function edit(Request $request, RessourceRepository $pRepo, Ville $ville): Response
    {
        $form = $this->createForm(VilleType::class, $ville);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ville_admin', array('id' => $ville->getRegion()->getId()));
        }

        $ressources = $pRepo->getNoParents();
        return $this->render('admin/gestionpays/ville/edit.html.twig', [
            'ressources' => $ressources,
            'region' => $ville->getRegion(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/add/{id<\d+>}", name="ville_add")
     */
    public function add(Request $request, RessourceRepository $pRepo, Region $region): Response
    {
        $ville = new Ville();
        $form = $this->createForm(VilleType::class, $ville);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ville->setRegion($region);
            $this->villeServices->execUpdate($ville);
            return $this->redirectToRoute('ville_admin', array('id' => $region->getId()));
        }

        $ressources = $pRepo->getNoParents();
        return $this->render('admin/gestionpays/ville/add.html.twig', [
            'region' => $region,
            'ressources' => $ressources,
            'form' => $form->createView(),
        ]);
    }
}