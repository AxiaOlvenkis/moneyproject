<?php

namespace App\Service;

use App\Entity\Ressource;
use App\Repository\PaysRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

class RessourceServices extends MainServices
{
    /** @var PaysRepository */
    private $ressourceRepository;
    /** UserPasswordEncoderInterface */

    /**
     * RessourceServices constructor.
     * @param EntityManagerInterface $em
     * @param ContainerInterface $container
     */
    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        parent::__construct($em, $container);
        $this->ressourceRepository = $em->getRepository(Ressource::class);
    }

    public function addRessource(array $posts){
        $ressource = new Ressource();
        $ressource->setNom($posts['nom']);
        $ressource->setRatio($posts['ratio']);
        $ressource->setEpuisable($posts['epuisable']);

        foreach($posts['parents'] as $p):
            $item = $this->ressourceRepository->find($p);
            $ressource->addParent($item);
        endforeach;

        //dump($ressource);die;

        $this->execUpdate($ressource);
    }

    public function updateRessource(Ressource $ressource, array $posts){
        //dump($posts);die;
        $ressource->setNom($posts['nom']);
        $ressource->setRatio($posts['ratio']);
        $ressource->setEpuisable($posts['epuisable']);

        $ressource->resetParents();
        foreach($posts['parents'] as $p):
            $item = $this->ressourceRepository->find($p);
            $ressource->addParent($item);
        endforeach;

        $this->execUpdate($ressource);
    }
}