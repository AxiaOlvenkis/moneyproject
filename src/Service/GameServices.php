<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

class GameServices extends MainServices
{
    /** @var EntityManagerInterface */
    protected $em;
    /** @var ContainerInterface */
    protected $container;

    protected $default_impot = 1;
    protected $perte_si_famine = -0.05;
    protected $gain_si_surplus = 0.05;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        parent::__construct($em, $container);
    }

    public function getImpot(){
        return $this->default_impot;
    }

    public function getSurplus(){
        return $this->gain_si_surplus;
    }

    public function getFamine(){
        return $this->perte_si_famine;
    }
}