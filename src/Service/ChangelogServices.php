<?php

namespace App\Service;

use App\Entity\Changelog;
use App\Entity\ChangelogBlock;
use App\Repository\ChangelogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

class ChangelogServices extends MainServices
{
    /** @var ChangelogRepository */
    private $changelogRepository;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        parent::__construct($em, $container);
        $this->changelogRepository = $em->getRepository(Changelog::class);
    }

    public function addChangelog(array $posts){
        $changelog = new Changelog();
        $date = new \DateTime($posts['published_at']);
        $changelog->setPublishedAt($date);
        $changelog->setIntitule($posts['intitule']);

        foreach($posts['changelogBlocks'] as $b):
            $block = new ChangelogBlock();
            $block->setTitre($b['titre']);
            $block->setTexte($b['texte']);
            $changelog->addChangelogBlock($block);
        endforeach;

        $this->execUpdate($changelog);
    }

    public function updatePays(Changelog $changelog, array $posts){
        $changelog->setPublishedAt($posts['published_at']);
        $changelog->setIntitule($posts['nom']);

        $this->execUpdate($changelog);
    }
}