<?php

namespace App\Service;

use App\Entity\Pays;
use App\Repository\PaysRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

class PaysServices extends MainServices
{
    /** @var PaysRepository */
    private $paysRepository;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        parent::__construct($em, $container);
        $this->paysRepository = $em->getRepository(Pays::class);
    }

    public function addPays(array $posts){
        $pays = new Pays();
        $pays->setNom($posts['nom']);

        $this->execUpdate($pays);
    }

    public function updatePays(Pays $pays, array $posts){
        $pays->setNom($posts['nom']);

        $this->execUpdate($pays);
    }
}