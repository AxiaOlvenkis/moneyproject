<?php

namespace App\Service;

use App\Entity\Pays;
use App\Entity\Region;
use App\Repository\RegionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

class RegionServices extends MainServices
{
    /** @var RegionRepository */
    private $regionRepository;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        parent::__construct($em, $container);
        $this->regionRepository = $em->getRepository(Region::class);
    }

    public function addRegion(array $posts, Pays $pays){
        $region = new Region();
        $region->setNom($posts['nom']);
        $region->setPays($pays);

        $this->execUpdate($region);
    }

    public function updateRegion(Region $region, array $posts){
        $region->setNom($posts['nom']);

        $this->execUpdate($region);
    }
}