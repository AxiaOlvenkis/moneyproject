<?php

namespace App\Service;

use App\Entity\Ressource;
use App\Entity\SocieteFiliales;
use App\Entity\TemporaryRecap;
use App\Entity\VilleRessources;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProductionServices extends SocietesServices
{
    protected $default_price = 2;
    protected $default_brut_production = 10000;
    protected $default_manufactured_production = 15000;
    protected $default_taux_investissement_brute = 0.001;
    protected $default_taux_investissement_manufactured = 0.04;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container, UserPasswordEncoderInterface $encoder, EncoderFactoryInterface $encoderFactory, GameServices $gameServices)
    {
        parent::__construct($em, $container, $encoder, $encoderFactory, $gameServices);
    }

    public function generate(){
        // on prend la liste de toutes filiales
        $liste = $this->em->getRepository(SocieteFiliales::class)->findAll();

        foreach($liste as $f){
            $temp = new TemporaryRecap();
            $temp->setFiliale($f);
            $temp->setPrixUnite($this->default_price);
            $vente = $f->getInvestissement() * -1;
            $temp->setVente($vente);
            $temp->setVille($f->getVille());
            $temp->setTauxstr('');
            $temp->setRessource($f->getActiveProduction()->getRessourcesSortantes());

            /**
             * 2 choix :
             * -> soit la filiale produit des ressources qui n'ont pas besoin d'autres ressources pour leurs productions (= ressources brutes = ressources DE LA ville)
             */
            $ressources = $f->getRessourcesFiliales();
            /** on parcours les ressources entrantes */
            foreach($ressources as $r){
                if($r->getVilleRessource() != null){
                    $production = $this->production_brute($temp, $r->getVilleRessource()) + $temp->getQuantite();
                }else{
                    $production = $this->production_manufacture($temp, $r->getRessource());
                }
                $temp->setQuantite($production);
            }

            $this->execUpdate($temp);
        }
    }

    /**
     * @param TemporaryRecap $temp
     * @param VilleRessources $vRessource
     * @return float|int
     */
    private function production_brute($temp, $vRessource){
        $percentEntreprise = $temp->getFiliale()->getType()->getRatioProduction() / 100;
        $investissement = $temp->getFiliale()->getInvestissement() * $this->default_taux_investissement_brute;
        $percentRessource = ($investissement + $vRessource->getFertilite() + $vRessource->getRessource()->getRatio()) / 100;
        $ratio = $percentEntreprise * $percentRessource;

        return $this->default_brut_production * $ratio;
    }

    /**
     * @param TemporaryRecap $temp
     * @param Ressource $ressource
     * @return float|int
     */
    private function production_manufacture($temp, $ressource){
        $percentEntreprise = $temp->getFiliale()->getType()->getRatioProduction() / 100;
        $investissement = $temp->getFiliale()->getInvestissement() * $this->default_taux_investissement_manufactured;
        $percentRessource = $ressource->getRatio() / 100;
        $ratio = $percentEntreprise * $percentRessource * $investissement;

        // on recupere toutes les ressources necessaires à la production dans la ville de la filiale pour cette societe
        $needed = $temp->getFiliale()->getActiveProduction()->getRessourcesEntrantes();
        $liste = $this->em->getRepository(SocieteFiliales::class)->filialeFromSocieteAndRessources($needed, $temp->getFiliale()->getSociete(), $temp->getFiliale()->getVille());
        $max = ($temp->getFiliale()->getNiveau() * 10) * $this->default_manufactured_production;

        $quantite = 0;
        /** @var SocieteFiliales $f */
        foreach($liste as $f){
            if($max > $f->getProduction()){
                $quantite += $f->getProduction();
                $max -= $f->getProduction();
                $reste = 0;
            }else{
                $quantite += $f->getProduction() - $max;
                $reste = $f->getProduction() - $max;
                $max = 0;
            }

            $f->setProduction($reste);
            $this->execUpdate($f);
        }

        return $quantite * $ratio;
    }
}