<?php

namespace App\Service;

use App\Entity\MarketFiliales;
use App\Entity\Pays;
use App\Entity\Ressource;
use App\Entity\RessourcesFiliales;
use App\Entity\SocieteFiliales;
use App\Entity\Societes;
use App\Entity\TemporaryFiliales;
use App\Entity\TemporaryRecap;
use App\Entity\TypeEntrepriseProduction;
use App\Entity\TypesEntreprises;
use App\Entity\Ville;
use App\Entity\VilleRessources;
use App\Repository\PaysRepository;
use App\Repository\RessourceRepository;
use App\Repository\SocietesRepository;
use App\Repository\TypesEntreprisesRepository;
use App\Repository\VilleRepository;
use App\Repository\VilleRessourcesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Psr\Container\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SocietesServices
{
    /** @var EntityManagerInterface */
    protected $em;
    /** @var ContainerInterface */
    protected $container;
    /** @var SocietesRepository */
    protected $societeRepository;
    /** @var VilleRepository */
    protected $villeRepository;
    /** @var RessourceRepository */
    protected $ressourcesRepository;
    /** @var VilleRessourcesRepository */
    protected $villeRessourcesRepository;
    /** @var TypesEntreprisesRepository */
    protected $typesEntreprisesRepository;
    /** UserPasswordEncoderInterface */
    protected $encoder;
    /** EncoderFactoryInterface */
    protected $encoderFactory;
    /** @var ProductionServices */
    protected $prodServices;
    /** @var GameServices */
    protected $gameServices;

    protected $default_taux_investissement = 0.001;
    protected $basique_filiale_price = 100000;
    //Ressource max utilisable pour la production
    protected $max_production_for_niveau_1 = 10000;
    protected $max_production_for_niveau_2 = 100000;
    protected $max_production_for_niveau_3 = 1000000;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container, UserPasswordEncoderInterface $encoder, EncoderFactoryInterface $encoderFactory, GameServices $gameServices, ProductionServices $prodServices = null)
    {
        $this->em = $em;
        $this->container = $container;
        $this->societeRepository = $em->getRepository(Societes::class);
        $this->villeRessourcesRepository = $em->getRepository(VilleRessources::class);
        $this->ressourcesRepository = $em->getRepository(Ressource::class);
        $this->typesEntreprisesRepository = $em->getRepository(TypesEntreprises::class);
        $this->villeRepository = $em->getRepository(Ville::class);
        $this->encoder = $encoder;
        $this->encoderFactory = $encoderFactory;
        $this->prodServices = $prodServices;
        $this->gameServices = $gameServices;
    }

    public function execUpdate($societe){
        $this->em->persist($societe);
        $this->em->flush();
    }

    public function delete_ia_entreprise(){
        $ia_societies = $this->societeRepository->findBy(array('user' => null));

        foreach ($ia_societies as $s):
            $this->em->remove($s);
        endforeach;
        $this->em->flush();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function generate_ia_entreprises(){
        $villes = $this->villeRepository->findAll();
        foreach($villes as $k => $v){
            $societe = $this->generate_default_single_society($k);

            // on recupere les ressources de la ville (qui ne sont pas lié à un utilisateur humain)
            $vRessources = $this->villeRessourcesRepository->findIARessources($v);
            foreach($vRessources as $vk => $vr){
                // on genere les filiales
                $this->generate_default_filiales($societe, $vr, null, null, "FilialeRandom" . $vk);
            }

            // on genere les entreprises agroalimentaires
            $ble = $this->em->getRepository(Ressource::class)->findOneBy(array('nom' => 'Blé'));
            $mais = $this->em->getRepository(Ressource::class)->findOneBy(array('nom' => 'Maïs'));
            // on verifie si la ville produit ces ressources
            $rBle = $this->em->getRepository(VilleRessources::class)->findOneBy(array('ressource' => $ble));
            $rMais = $this->em->getRepository(VilleRessources::class)->findOneBy(array('ressource' => $mais));

            if($rBle != null):
                $bleR = $this->em->getRepository(Ressource::class)->findOneBy(array('nom' => 'Blé Récolté'));
                $societe = $this->generate_default_filiales($societe, null, $v, $bleR, "FilialeBle");
            endif;

            if($rMais != null):
                $maisR = $this->em->getRepository(Ressource::class)->findOneBy(array('nom' => 'Maïs Récolté'));
                $societe = $this->generate_default_filiales($societe, null, $v, $maisR, "FilialeMais");
            endif;

            $this->execUpdate($societe);
        }
    }

    public function generate_default_single_society($nb, $name = 'RandomSociete', $isIA = true){
        $societe = new Societes();

        if($isIA){
            $societe->setNom($name . $nb);
            $argent = rand(1,10) * 10000;
        }else{
            $societe->setNom($name);
            $argent = 10000;
        }

        $societe->setArgent($argent);

        return $societe;
    }

    /**
     * @param Societes $societe
     * @param VilleRessources $ressourceVille
     * @param Ville $ville
     * @param Ressource $nRessource
     * @param string $name
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function generate_default_filiales($societe, $ressourceVille = null, $ville = null, $nRessource = null, $name = "Random"){
        $filiale = new SocieteFiliales();
        $filiale->setNom($name);
        $argent = rand(1,10) * 1000;
        $filiale->setArgent($argent);
        $inv = $argent / 2;
        $filiale->setInvestissement($inv);
        $filiale->setNiveau(1);
        $filiale->setProduction(0);

        $fRessource = new RessourcesFiliales();
        if($ressourceVille != null):
            $ressource = $ressourceVille->getRessource();
            $ville = $ressourceVille->getVille();
            $fRessource->setVilleRessource($ressourceVille);
        else:
            $ressource = $nRessource;
            $fRessource->setRessource($nRessource);
        endif;

        $type = $this->typesEntreprisesRepository->getTypeForRessource($ressource);
        if($type == null):
            $type = $this->typesEntreprisesRepository->getTypeForRessourceSortante($ressource);
        endif;
        $filiale->setType($type);
        $filiale->setVille($ville);
        $filiale->addRessourcesFiliale($fRessource);

        // on recupere la production active (TypeEntrepriseProduction)
        /*$typeProd = $type->getProductions();
        foreach($typeProd as $k => $tp){
            $filiale->setActiveProduction($tp);
        }*/
        $coll = array($ressource->getId());
        $typeProd = $this->em->getRepository(TypeEntrepriseProduction::class)->getByTypeAndEntrant($type, $coll);
        $filiale->setActiveProduction($typeProd);

        $societe->addSocieteFiliale($filiale);

        return $societe;
    }

    public function sendIAtoMarket(){
        $ia_societies = $this->societeRepository->findBy(array('user' => null));

        foreach ($ia_societies as $s):
            $filiales = $s->getSocieteFiliales();
            foreach ($filiales as $f):
                $market = new MarketFiliales();
                $market->setFiliale($f);

                $rand = rand(80,300); // pourcentage random
                $percent = $rand / 100;
                $price_base = $this->basique_filiale_price * $f->getNiveau(); // le prix de base * le niveau de la filiale
                $price = $price_base * $percent;

                $market->setPrix($price);

                $this->execUpdate($market);
            endforeach;
        endforeach;
    }

    /**
     * @param Societes $societe
     * @param string $nom
     * @param TypesEntreprises $type
     * @param Ville $ville
     * @param VilleRessources $vRessource
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function create_filiale($societe, $nom, $type, $ville, $vRessource = null){
        $filiale = new SocieteFiliales();
        $filiale->setNom($nom);
        $filiale->setVille($ville);
        $filiale->setArgent(0);
        $filiale->setInvestissement(0);
        $filiale->setNiveau(1);
        $filiale->setProduction(0);
        $filiale->setType($type);

        /** On crée la 'ressource filiale */
        /** si il y a une ressource, on est sur de l'exploitation primaire, sinon c'est l'utilisateur qui choisit la ressource entrante - donc plus tard - */
        /** Production Active : set si ressource primaire, car unique */
        if($vRessource):
            /** il faut nettoyer l'emplacement si il existe une entreprise IA sur cette ressource */
            if($vRessource->getProdFiliales() && $vRessource->getProdFiliales()->getFiliale()):
                $ia_filiale = $vRessource->getProdFiliales()->getFiliale();
                $this->em->remove($ia_filiale);
                $this->em->flush();
            endif;
            /** */
            $fRessource = new RessourcesFiliales();
            $fRessource->setVilleRessource($vRessource);
            $filiale->addRessourcesFiliale($fRessource);

            $coll = array($vRessource->getRessource()->getId());
            $typeProd = $this->em->getRepository(TypeEntrepriseProduction::class)->getByTypeAndEntrant($type, $coll);
            $filiale->setActiveProduction($typeProd);
        endif;

        /** fin de la fonction */
        $societe->addSocieteFiliale($filiale);
        return $societe;
    }

    /**
     * @param TemporaryRecap $temp
     */
    public function maj_filiale($temp){
        $filiale = $temp->getFiliale();

        $argent = $filiale->getArgent() + $temp->getVente();
        $filiale->setArgent($argent);
        $filiale->setProduction($temp->getQuantite());

        $this->execUpdate($filiale);
    }

    /**
     * @param SocieteFiliales $filiale
     */
    public function daily_production($filiale){
        $str_max = "max_production_for_niveau_" . $filiale->getNiveau();
        $max_prod = $this->$str_max;
        $investissement = $filiale->getInvestissement() * 0.001;
        $tempo = new TemporaryRecap();
        $tempo->setVille($filiale->getVille());
        $tempo->setFiliale($filiale);
        $tempo->setSociete($filiale->getSociete());
        $tempo->setUser($filiale->getSociete()->getUser());
        $tempo->setQuantite(0);

        $fRessources = $filiale->getRessourcesFiliales();
        foreach($fRessources as $fr):
            // 2 choix : soit la production demande des ressources (ressources), dans ce cas, on achete, soit la production n'en demande pas (villeRessources)
            if($fr->getRessource() != null):
                $ressource = $fr->getRessource();
                $taux_production = ($filiale->getType()->getRatioProduction() / 100) * (1 + (($investissement + $ressource->getRatio() / 100)));
                $str_taux = "(" . $filiale->getType()->getRatioProduction() . " / 100) * (1 + ((" . $investissement . " + " .  $ressource->getRatio() . " / 100)))";
                $tempo->setTauxstr($str_taux);
                $tempo = $this->prodServices->manufacturedRessources($tempo, $ressource, $filiale, $taux_production);
            else:
                $vRessource = $fr->getVilleRessource();
                $ressource = $vRessource->getRessource();
                $taux_production = ($filiale->getType()->getRatioProduction() / 100) * (1 + (($investissement + $vRessource->getFertilite() + $ressource->getRatio()) / 100));
                $str_taux = "(" . $filiale->getType()->getRatioProduction() . " / 100) * (1 + ((" . $investissement . " + " .  $vRessource->getFertilite() . " + " . $ressource->getRatio() . ") / 100)))";

                $tempo->setTauxstr($str_taux);
                $tempo = $this->prodServices->recolteRessource($tempo, $ressource, $taux_production);
            endif;
        endforeach;

        $this->gameServices->update_temp_recap($tempo);
    }
}