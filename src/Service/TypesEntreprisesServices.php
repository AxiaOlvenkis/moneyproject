<?php

namespace App\Service;

use App\Entity\Ressource;
use App\Entity\TypeEntrepriseProduction;
use App\Entity\TypesEntreprises;
use App\Repository\VilleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

class TypesEntreprisesServices extends MainServices
{
    /** @var VilleRepository */
    private $typesEntreprisesRepository;

    /**
     * TypesEntreprisesServices constructor.
     * @param EntityManagerInterface $em
     * @param ContainerInterface $container
     */
    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        parent::__construct($em, $container);
        $this->typesEntreprisesRepository = $em->getRepository(TypesEntreprises::class);
    }

    public function addTypesEntreprises(array $posts){
        $type = new TypesEntreprises();
        $type->setNom($posts['nom']);
        $type->setRatioProduction($posts['ratio']);
        $nb = $posts['nbtabline'];

        for($i = 1; $i <= $nb; $i++):
            $production = new TypeEntrepriseProduction();
            $rRepository = $this->em->getRepository(Ressource::class);

            foreach($posts['ressources-in-'.$i] as $idR):
                $r = $rRepository->find($idR);
                $production->addRessourcesEntrante($r);
            endforeach;

            $r = $rRepository->find($posts['ressources-out-'.$i]);
            $production->setRessourcesSortantes($r);

            $type->addProduction($production);
        endfor;

        $this->execUpdate($type);
    }

    public function updateTypesEntreprises(TypesEntreprises $type, array $posts){
        $type->setNom($posts['nom']);
        $type->setRatioProduction($posts['ratio']);

        $type->resetProductions();
        $nb = $posts['nbtabline'];

        for($i = 1; $i <= $nb; $i++):
            $production = new TypeEntrepriseProduction();
            $rRepository = $this->em->getRepository(Ressource::class);

            foreach($posts['ressources-in-'.$i] as $idR):
                $r = $rRepository->find($idR);
                $production->addRessourcesEntrante($r);
            endforeach;

            $r = $rRepository->find($posts['ressources-out-'.$i]);
            $production->setRessourcesSortantes($r);

            $type->addProduction($production);
        endfor;
        //dump($type);die;

        $this->execUpdate($type);
    }
}