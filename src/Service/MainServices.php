<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

class MainServices
{
    /** @var EntityManagerInterface */
    protected $em;
    /** @var ContainerInterface */
    protected $container;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function execUpdate($item){
        $this->em->persist($item);
        $this->em->flush();
    }
}