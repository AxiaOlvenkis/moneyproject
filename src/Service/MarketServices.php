<?php

namespace App\Service;

use App\Entity\MarketFiliales;
use App\Entity\MarketRessources;
use App\Entity\RessourcesFiliales;
use App\Entity\SocieteFiliales;
use App\Entity\Societes;
use App\Entity\TemporaryRecap;
use App\Entity\VilleRessources;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Integer;
use Psr\Container\ContainerInterface;

class MarketServices extends MainServices
{
    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        parent::__construct($em, $container);
    }

    public function buyEmplacementRessource(VilleRessources $vRessource, Float $prix, SocieteFiliales $acheteur){
        $ville = $vRessource->getVille();

        // on met à jour l'argent de la ville
        $money = $ville->getArgent() + $prix;
        $ville->setArgent($money);

        // on met à jour la filiale
        $f_money = $acheteur->getArgent() - $prix;
        $acheteur->setArgent($f_money);

        $rFiliale = new RessourcesFiliales();
        $rFiliale->setVilleRessource($vRessource);

        $acheteur->addRessourcesFiliale($rFiliale);

        $this->execUpdate($ville);
        $this->execUpdate($acheteur);
    }

    public function buyRessources(MarketRessources $mRessources, Integer $quantite, SocieteFiliales $acheteur){
        $vendeur = $mRessources->getFiliale();

        // on ajoute l'argent au vendeur
        $gain = $vendeur->getArgent() + ($mRessources->getPrixUnitaire() * $quantite);
        $vendeur->setArgent($gain);

        // on retire l'argent a l'acheteur
        $perte = $acheteur->getArgent() - ($mRessources->getPrixUnitaire() * $quantite);
        $acheteur->setArgent($perte);

        // on ajoute les ressources à la production du jour de l'acheteur
        $temp = new TemporaryRecap();
        $temp->setFiliale($acheteur);
        $temp->setSociete($acheteur->getSociete());
        $temp->setVille($mRessources->getVille());
        $temp->setUser($acheteur->getSociete()->getUser());
        $temp->setPrixUnite(2);
        $vente = $acheteur->getInvestissement() * -1;
        $temp->setVente($vente);
        $temp->setTauxstr('');
        $temp->setRessource($mRessources->getRessource());

        $this->execUpdate($vendeur);
        $this->execUpdate($acheteur);
        $this->execUpdate($temp);

        // on supprime la ressource market
        $this->em->remove($mRessources);
        $this->em->flush();
    }

    public function buyFiliale(MarketFiliales $mFiliale, Societes $acheteur){
        $filiale = $mFiliale->getFiliale();
        $vendeur = $filiale->getSociete();

        // on ajoute l'argent au vendeur et on recupere l'argent encore en caisse dans la filiale vendu (même si il s'agit d'une dette)
        $gain = $vendeur->getArgent() + $mFiliale->getPrix() + $filiale->getArgent();
        $vendeur->setArgent($gain);

        // on retire l'argent a l'acheteur
        $perte = $acheteur->getArgent() - $mFiliale->getPrix();
        $acheteur->setArgent($perte);

        // on modifie la filiale
        $filiale->setArgent(0);
        $filiale->setInvestissement(0);
        $filiale->setSociete($acheteur);

        $this->execUpdate($vendeur);
        $this->execUpdate($acheteur);
        $this->execUpdate($filiale);

        // on supprime la ressource market
        $this->em->remove($mFiliale);
        $this->em->flush();
    }
}