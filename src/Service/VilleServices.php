<?php

namespace App\Service;

use App\Entity\Ressource;
use App\Entity\TemporaryRecap;
use App\Entity\TemporaryVilles;
use App\Entity\Ville;
use App\Entity\Region;
use App\Entity\VilleRessources;
use App\Repository\VilleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

class VilleServices extends MainServices
{
    /** @var VilleRepository */
    private $villeRepository;
    /** @var SocietesServices */
    private $societeServices;
    /** @var GameServices */
    private $gameServices;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container, SocietesServices $societeServices, GameServices $gameServices)
    {
        parent::__construct($em, $container);
        $this->villeRepository = $em->getRepository(Ville::class);
        $this->societeServices = $societeServices;
        $this->gameServices = $gameServices;
    }

    public function addVille(array $posts, Region $region){
        $ville = new Ville();
        $ville->setNom($posts['nom']);
        $ville->setArgent($posts['argent']);
        $ville->setPopulation($posts['population']);
        $ville->setRegion($region);

        $nb = $posts['nbtabline'];
        for($i = 1; $i <= $nb; $i++):
            $vressource = new VilleRessources();
            $vressource->setFertilite($posts['fertilite-'.$i]);
            $vressource->setQuantite($posts['quantite-'.$i]);

            $rRepository = $this->em->getRepository(Ressource::class);
            $ressource = $rRepository->find($posts['ressources-'.$i]);
            $vressource->setRessource($ressource);
            $ville->addVilleRessource($vressource);
        endfor;

        $this->execUpdate($ville);
    }

    public function updateVille(Ville $ville, array $posts){
        $ville->setNom($posts['nom']);
        $ville->setArgent($posts['argent']);
        $ville->setPopulation($posts['population']);

        $ville->resetRessources();
        $nb = $posts['nbtabline'];
        for($i = 1; $i <= $nb; $i++):
            $vressource = new VilleRessources();
            $vressource->setFertilite($posts['fertilite-'.$i]);
            $vressource->setQuantite($posts['quantite-'.$i]);

            $rRepository = $this->em->getRepository(Ressource::class);
            $ressource = $rRepository->find($posts['ressources-'.$i]);
            $vressource->setRessource($ressource);
            $ville->addVilleRessource($vressource);
        endfor;

        $this->execUpdate($ville);
    }

    /**
     * @param Ville $ville
     *
     */
    public function dayCity($ville){
        /** On update l'argent' */
        $argent = $ville->getPopulation() * $this->gameServices->getImpot(); // par défaut, la ville gagne X par jour par habitant

        /** On nourrit la ville pour connaitre ses gains/pertes en population */
        $population = $this->nourrirPopulation($ville);

        $tempVille = new TemporaryVilles();
        $tempVille->setVille($ville);
        $tempVille->setArgent($argent);
        $tempVille->setPopulation($population);
        $this->execUpdate($tempVille);
    }

    /**
     * @param Ville $ville
     * @return float|int
     */
    public function nourrirPopulation($ville){
        $reste_a_nourrir = $ville->getPopulation();
        $surplus = 0;

        // on regarde la nourriture disponible dans la ville, et on l'achete, en commençant par les prix les plus bas.
        $n = $this->em->getRepository(Ressource::class)->findOneBy(array('nom' => 'Nourriture Simple'));
        $rRepository = $this->em->getRepository(TemporaryRecap::class);
        $bouffes = $rRepository->findBy(array('ville' => $ville, 'ressource' => $n), array('prix_unite' => 'DESC'));

        foreach($bouffes as $k => $b){
            // il reste des personnes a nourrir
            if($reste_a_nourrir > 0){
                // 1. il y a plus de nourriture dispo que de personnes a nourrir
                // 2. il y en a moins
                $gain = $b->getQuantite() * $b->getPrixUnite();
                if($reste_a_nourrir < $b->getQuantite()){
                    $reste = $b->getQuantite() - $reste_a_nourrir;
                    $b->setQuantite($reste);
                    $reste_a_nourrir = 0;
                }else{
                    $reste_a_nourrir = $reste_a_nourrir - $b->getQuantite();
                    $b->setQuantite(0);
                }
                $b->setVente($gain);
            }else{
                $vendu = $b->getQuantite() * 0.25;
                $reste = $b->getQuantite() - $vendu;
                $gain = $vendu * $b->getPrixUnite();
                $surplus += $vendu;

                $b->setQuantite($reste);
                $b->setVente($gain);
            }

            // on update la ligne
            $this->execUpdate($b);
        }

        // on a fini de nourrir
        // si il reste des gens à nourrir, on diminue la population de 5% de ce reste à nourrir
        // sinon, on augmente la population de 5% du surplus de nourriture
        if($reste_a_nourrir > 0):
            $population = $reste_a_nourrir * $this->gameServices->getFamine();
        else:
            $population = $surplus * $this->gameServices->getSurplus();
        endif;

        return $population;
    }

    /**
     * @param TemporaryVilles $temp
     */
    public function maj_city($temp){
        $ville = $temp->getVille();

        $argent = $ville->getArgent() + $temp->getArgent();
        $pop = $ville->getPopulation() + $temp->getPopulation();

        $ville->setArgent($argent);
        $ville->setPopulation($pop);

        $this->execUpdate($ville);
    }
}