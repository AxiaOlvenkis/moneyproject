<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserServices extends MainServices
{
    /** @var UserRepository */
    private $userRepository;
    /** UserPasswordEncoderInterface */
    private $encoder;
    /** EncoderFactoryInterface */
    private $encoderFactory;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container, UserPasswordEncoderInterface $encoder, EncoderFactoryInterface $encoderFactory)
    {
        parent::__construct($em, $container);
        $this->userRepository = $em->getRepository(User::class);
        $this->encoder = $encoder;
        $this->encoderFactory = $encoderFactory;
    }

    public function updateUser(User $user, array $posts){
        $user->setUsername($posts['username']);
        $user->setEmail($posts['email']);

        $droits = $posts['roles'];
        $user->resetRoles();
        $user->setRoles($droits);

        $pw = $posts['password'];
        if($pw != ""){
            $user->setPassword($this->encoder->encodePassword($user, $pw));
        }

        $this->execUpdate($user);
    }
}