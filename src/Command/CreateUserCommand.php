<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'create:user';
    /** @var EntityManagerInterface */
    protected $em;
    /** UserPasswordEncoderInterface */
    private $encoder;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct();
        $this->em = $em;
        $this->encoder = $encoder;
    }

    protected function configure()
    {
        $this
            ->setDescription('Generate an admin user')
            ->addArgument('email', InputArgument::OPTIONAL, 'Email utilisateur')
            ->addArgument('pseudo', InputArgument::OPTIONAL, 'Pseudo utilisateur')
            ->addArgument('password', InputArgument::OPTIONAL, 'Mot de passe utilisateur')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');
        $name = $input->getArgument('pseudo');
        $pw = $input->getArgument('password');

        if (!$email || !$name || !$pw) {
            $io->error('un argument est manquant !');
        }else{
            $user = new User();
            $user->setEmail($email);
            $user->setFullName($name);
            $user->setUsername(strtolower($name));
            $user->setPlainPassword($pw);
            $user->setRoles(array('ROLE_ADMIN'));
            $user->setPassword($this->encoder->encodePassword($user, $pw));
            $this->em->persist($user);
            $this->em->flush();

            $io->success('Utilisateur crée avec succés ! :)');
        }

        return 0;
    }
}
