<?php

namespace App\Command;

use App\Entity\Pays;
use App\Entity\Region;
use App\Entity\Ressource;
use App\Entity\SocieteFiliales;
use App\Entity\Societes;
use App\Entity\TemporaryRecap;
use App\Entity\TemporaryVilles;
use App\Entity\TypeEntrepriseProduction;
use App\Entity\TypesEntreprises;
use App\Entity\Ville;
use App\Entity\VilleRessources;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateGameCommand extends Command
{
    protected static $defaultName = 'create:game';
    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Génerer un nouveau jeu');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->newgame();

        $io->success('Données de jeu regeneré avec succés !');
    }

    private function newgame(){
        /**
         * on recupere les repository necessaires
         */
        $rRepository = $this->em->getRepository(Ressource::class);
        $paysRepository = $this->em->getRepository(Pays::class);
        $regionRepository = $this->em->getRepository(Region::class);
        $villeRepository = $this->em->getRepository(Ville::class);
        $tempVRepository = $this->em->getRepository(TemporaryVilles::class);
        $tempRRepository = $this->em->getRepository(TemporaryRecap::class);
        $socRepository = $this->em->getRepository(Societes::class);
        $typeRepository = $this->em->getRepository(TypesEntreprises::class);

        /** ON vide la base **/
        /** Table Temporaire */
        /* Ville */
        $listeTV = $tempVRepository->findAll();
        foreach($listeTV as $tv):
            $this->em->remove($tv);
        endforeach;
        /* Global */
        $listeTR = $tempRRepository->findAll();
        foreach($listeTR as $tr):
            $this->em->remove($tr);
        endforeach;
        /* Societe */
        $listeS = $socRepository->findAll();
        foreach($listeS as $s):
            $this->em->remove($s);
        endforeach;
        /* Table Ville */
        $listeV = $villeRepository->findAll();
        foreach($listeV as $v):
            $this->em->remove($v);
        endforeach;
        /* Table Region */
        $listeR = $regionRepository->findAll();
        foreach($listeR as $r):
            $this->em->remove($r);
        endforeach;
        /* Table Pays */
        $listeP = $paysRepository->findAll();
        foreach($listeP as $p):
            $this->em->remove($p);
        endforeach;
        /* Table Ressources */
        $listeR = $rRepository->findAll();
        foreach($listeR as $r):
            $this->em->remove($r);
        endforeach;
        /* Table Types Entreprises */
        $listeT = $typeRepository->findAll();
        foreach($listeT as $t):
            $this->em->remove($t);
        endforeach;
        $this->em->flush();
        /** *************************** */

        $this->generate_ressources();
        $this->generate_type_entreprise();

        $ble = $rRepository->findOneBy(array('nom' => 'Blé'));
        $mais = $rRepository->findOneBy(array('nom' => 'Maïs'));
        $fer = $rRepository->findOneBy(array('nom' => 'Fer'));

        // On genere les pays
        $pays = new Pays();
        $pays->setNom('Vierysas');

        // on genere les regions, et on les assigne à un pays
        $region1 = new Region();
        $region1->setNom('Castellino');

        $region2 = new Region();
        $region2->setNom('Erevidos');
        $region2->setPays($pays);

        $pays->addRegion($region1);
        $pays->addRegion($region2);

        // on genere les villes et on les assigne a une région
        $ville1 = new Ville();
        $ville1->setNom('Castellino')->setArgent(100000)->setPopulation(10000)->setRegion($region1);
        $r1 = new VilleRessources();
        $r1->setRessource($ble)->setQuantite(0)->setFertilite(100);
        $r2 = new VilleRessources();
        $r2->setRessource($ble)->setQuantite(0)->setFertilite(100);
        $r3 = new VilleRessources();
        $r3->setRessource($ble)->setQuantite(0)->setFertilite(80);
        $r4 = new VilleRessources();
        $r4->setRessource($ble)->setQuantite(0)->setFertilite(50);
        $r5 = new VilleRessources();
        $r5->setRessource($ble)->setQuantite(0)->setFertilite(30);
        $r6 = new VilleRessources();
        $r6->setRessource($mais)->setQuantite(0)->setFertilite(100);
        $r7 = new VilleRessources();
        $r7->setRessource($mais)->setQuantite(0)->setFertilite(80);
        $r8 = new VilleRessources();
        $r8->setRessource($mais)->setQuantite(0)->setFertilite(80);
        $r9 = new VilleRessources();
        $r9->setRessource($mais)->setQuantite(0)->setFertilite(50);
        $r10 = new VilleRessources();
        $r10->setRessource($mais)->setQuantite(0)->setFertilite(50);
        $ville1->addVilleRessource($r1)->addVilleRessource($r2)->addVilleRessource($r3)->addVilleRessource($r4)->addVilleRessource($r5)->addVilleRessource($r6)->addVilleRessource($r7)->addVilleRessource($r8)->addVilleRessource($r9)->addVilleRessource($r10);

        $ville2 = new Ville();
        $ville2->setNom('Yaltaï')->setArgent(50000)->setPopulation(5000);
        $r11 = new VilleRessources();
        $r11->setRessource($ble)->setQuantite(0)->setFertilite(50);
        $r12 = new VilleRessources();
        $r12->setRessource($mais)->setQuantite(0)->setFertilite(30);
        $r13 = new VilleRessources();
        $r13->setRessource($mais)->setQuantite(0)->setFertilite(30);
        $r14 = new VilleRessources();
        $r14->setRessource($fer)->setQuantite(100000)->setFertilite(100);
        $r15 = new VilleRessources();
        $r15->setRessource($fer)->setQuantite(75000)->setFertilite(100);
        $r16 = new VilleRessources();
        $r16->setRessource($fer)->setQuantite(75000)->setFertilite(75);
        $r17 = new VilleRessources();
        $r17->setRessource($fer)->setQuantite(50000)->setFertilite(60);
        $r18 = new VilleRessources();
        $r18->setRessource($fer)->setQuantite(50000)->setFertilite(50);
        $ville2->addVilleRessource($r11)->addVilleRessource($r12)->addVilleRessource($r13)->addVilleRessource($r14)->addVilleRessource($r15)->addVilleRessource($r16)->addVilleRessource($r17)->addVilleRessource($r18);

        $region1->addVille($ville1)->addVille($ville2);
        $this->em->persist($pays);
        $this->em->flush();

        return true;
    }

    private function generate_ressources(){
        $r1 = new Ressource();
        $r1->setNom('Blé');
        $r1->setRatio(10);
        $r1->setEpuisable(false);
        $this->em->persist($r1);

        $r2 = new Ressource();
        $r2->setNom('Maïs');
        $r2->setRatio(15);
        $r2->setEpuisable(false);
        $this->em->persist($r2);

        $r3 = new Ressource();
        $r3->setNom('Fer');
        $r3->setRatio(20);
        $r3->setEpuisable(true);
        $this->em->persist($r3);

        $r4 = new Ressource();
        $r4->setNom('Blé récolté');
        $r4->setRatio(10);
        $r4->setEpuisable(false);
        $r4->addParent($r1);
        $this->em->persist($r4);

        $r5 = new Ressource();
        $r5->setNom('Maïs récolté');
        $r5->setRatio(15);
        $r5->setEpuisable(false);
        $r5->addParent($r2);
        $this->em->persist($r5);

        $r6 = new Ressource();
        $r6->setNom('Fer récolté');
        $r6->setRatio(10);
        $r6->setEpuisable(false);
        $r6->addParent($r3);
        $this->em->persist($r6);

        $r7 = new Ressource();
        $r7->setNom('Nourriture Simple');
        $r7->setRatio(10);
        $r7->setEpuisable(false);
        $r7->addParent($r4);
        $r7->addParent($r5);
        $this->em->persist($r7);

        $this->em->flush();
    }

    private function generate_type_entreprise(){
        $rRepository = $this->em->getRepository(Ressource::class);
        $ble = $rRepository->findOneBy(array('nom' => 'Blé'));
        $mais = $rRepository->findOneBy(array('nom' => 'Maïs'));
        $bleR = $rRepository->findOneBy(array('nom' => 'Blé récolté'));
        $maisR = $rRepository->findOneBy(array('nom' => 'Maïs récolté'));
        $fer = $rRepository->findOneBy(array('nom' => 'Fer'));
        $ferR = $rRepository->findOneBy(array('nom' => 'Fer récolté'));
        $bouffeS = $rRepository->findOneBy(array('nom' => 'Nourriture Simple'));

        $t1 = new TypesEntreprises();
        $t1->setNom('Ferme');
        $t1->setRatioProduction(20);
        $t1R1 = new TypeEntrepriseProduction();
        $t1R1->addRessourcesEntrante($ble);
        $t1R1->setRessourcesSortantes($bleR);
        $t1R2 = new TypeEntrepriseProduction();
        $t1R2->addRessourcesEntrante($mais);
        $t1R2->setRessourcesSortantes($maisR);
        $t1->addProduction($t1R1);
        $t1->addProduction($t1R2);
        $this->em->persist($t1);

        $t2 = new TypesEntreprises();
        $t2->setNom('Mine');
        $t2->setRatioProduction(15);
        $t2R1 = new TypeEntrepriseProduction();
        $t2R1->addRessourcesEntrante($fer);
        $t2R1->setRessourcesSortantes($ferR);
        $t2->addProduction($t2R1);
        $this->em->persist($t2);

        $t3 = new TypesEntreprises();
        $t3->setNom('	Usine AgroAlimentaire');
        $t3->setRatioProduction(20);
        $t3R1 = new TypeEntrepriseProduction();
        $t3R1->addRessourcesEntrante($bleR);
        $t3R1->setRessourcesSortantes($bouffeS);
        $t3R2 = new TypeEntrepriseProduction();
        $t3R2->addRessourcesEntrante($maisR);
        $t3R2->setRessourcesSortantes($bouffeS);
        $t3->addProduction($t3R1);
        $t3->addProduction($t3R2);
        $this->em->persist($t3);

        $this->em->flush();
    }
}
