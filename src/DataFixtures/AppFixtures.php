<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;

class AppFixtures extends Fixture implements FixtureGroupInterface
{

    public static function getGroups(): array
     {
         return ['usergroup'];
     }

    public function load(ObjectManager $manager): void
    {
        $this->loadUsers($manager);
    }

    private function loadUsers(ObjectManager $manager): void
    {

        $manager->flush();
    }

    private function getUserData(): array
    {
        return [
            //[$fullname, $username, $password, $email, $roles];
            ['Tio', 'tio', 'kitten', 'uccellounet@gmail.com', ['ROLE_USER']],
            ['Random', 'random', 'kitten', 'random@gmail.com', ['ROLE_USER']],
        ];
    }
}
