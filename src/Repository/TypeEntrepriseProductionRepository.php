<?php

namespace App\Repository;

use App\Entity\TypeEntrepriseProduction;
use App\Entity\TypesEntreprises;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeEntrepriseProduction|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeEntrepriseProduction|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeEntrepriseProduction[]    findAll()
 * @method TypeEntrepriseProduction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeEntrepriseProductionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeEntrepriseProduction::class);
    }

    /**
     * @param TypesEntreprises $type
     * @param array $entrants
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getByTypeAndEntrant($type, $entrants){
        $qb = $this->_em->createQueryBuilder();

        $typeProd = $qb->select('tp')
            ->from($this->_entityName, 'tp')
            ->innerJoin('tp.ressourcesEntrantes','r')
            ->where('tp.typesEntreprises = :type')
            /*->andWhere('tp.ressourcesEntrantes IN (:ressources)')*/
            ->andWhere('r.id IN (:ressources)')
            ->setParameter("type", $type)
            ->setParameter("ressources", $entrants)
            ->getQuery()->getOneOrNullResult();

        return $typeProd;
    }

    // /**
    //  * @return TypeEntrepriseProduction[] Returns an array of TypeEntrepriseProduction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeEntrepriseProduction
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
