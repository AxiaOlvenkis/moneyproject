<?php

namespace App\Repository;

use App\Entity\TemporaryRecap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TemporaryRecap|null find($id, $lockMode = null, $lockVersion = null)
 * @method TemporaryRecap|null findOneBy(array $criteria, array $orderBy = null)
 * @method TemporaryRecap[]    findAll()
 * @method TemporaryRecap[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TemporaryRecapRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TemporaryRecap::class);
    }

    // /**
    //  * @return TemporaryRecap[] Returns an array of TemporaryRecap objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TemporaryRecap
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
