<?php

namespace App\Repository;

use App\Entity\Ressource;
use App\Entity\TypesEntreprises;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypesEntreprises|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypesEntreprises|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypesEntreprises[]    findAll()
 * @method TypesEntreprises[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypesEntreprisesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypesEntreprises::class);
    }

    /**
     * @param Ressource $ressource
     * @return TypesEntreprises $type;
     * @throws \Doctrine\ORM\NonUniqueResultException*
     */
    public function getTypeForRessource($ressource){
        $qb = $this->_em->createQueryBuilder();

        $type = $qb->select('t')
                    ->from($this->_entityName, 't')
                    ->innerJoin('t.productions','p')
                    ->innerJoin('p.ressourcesEntrantes','r')
                    ->where('r.nom = :nameR')
                    ->setParameter("nameR", $ressource->getNom())
                    ->getQuery()->getOneOrNullResult();

        return $type;
    }

    /**
     * @param Ressource $ressource
     * @return TypesEntreprises $type;
     * @throws \Doctrine\ORM\NonUniqueResultException*
     */
    public function getTypeForRessourceSortante($ressource){
        $qb = $this->_em->createQueryBuilder();

        $type = $qb->select('t')
            ->from($this->_entityName, 't')
            ->innerJoin('t.productions','p')
            ->innerJoin('p.ressourcesSortantes','r')
            ->where('r.nom = :nameR')
            ->setParameter("nameR", $ressource->getNom())
            ->getQuery()->getOneOrNullResult();

        return $type;
    }

    // /**
    //  * @return TypesEntreprises[] Returns an array of TypesEntreprises objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypesEntreprises
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
