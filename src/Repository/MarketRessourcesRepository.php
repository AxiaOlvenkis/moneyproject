<?php

namespace App\Repository;

use App\Entity\MarketRessources;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MarketRessources|null find($id, $lockMode = null, $lockVersion = null)
 * @method MarketRessources|null findOneBy(array $criteria, array $orderBy = null)
 * @method MarketRessources[]    findAll()
 * @method MarketRessources[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MarketRessourcesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MarketRessources::class);
    }

    // /**
    //  * @return MarketRessources[] Returns an array of MarketRessources objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MarketRessources
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
