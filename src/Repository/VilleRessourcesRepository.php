<?php

namespace App\Repository;

use App\Entity\VilleRessources;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VilleRessources|null find($id, $lockMode = null, $lockVersion = null)
 * @method VilleRessources|null findOneBy(array $criteria, array $orderBy = null)
 * @method VilleRessources[]    findAll()
 * @method VilleRessources[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VilleRessourcesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VilleRessources::class);
    }

    public function findFree($ville, $entrants){
        $qb = $this->_em->createQueryBuilder();

        $qb->select('vr')->from($this->_entityName, 'vr');
        $qb->innerJoin('vr.prodFiliales','pf');
        $qb->innerJoin('vr.ressource','r');
        $qb->innerJoin('pf.filiale','f');
        $qb->innerJoin('f.societe','s');
        $qb->where('s.user is NULL');
        $qb->andWhere('r.id IN (:ressources)');
        $qb->setParameter("ressources", $entrants);
        $qb->andWhere('vr.ville IN (:ville)');
        $qb->setParameter("ville", $ville);

        return $qb->getQuery()->getResult();
    }

    public function findIARessources($ville = null){
        $qb = $this->_em->createQueryBuilder();

        $qb->select('vr')->from($this->_entityName, 'vr');
        $qb->leftJoin('vr.prodFiliales','pf');
        $qb->leftJoin('vr.ressource','r');
        $qb->leftJoin('pf.filiale','f');
        $qb->leftJoin('f.societe','s');
        $qb->where('s.user is NULL');

        if($ville != null):
            $qb->andWhere('vr.ville = :ville');
            $qb->setParameter("ville", $ville);
        endif;

        return $qb->getQuery()->getResult();
    }

    public function findEmptyRessource($ville = null){
        $qb = $this->_em->createQueryBuilder();

        $qb->select('vr')->from($this->_entityName, 'vr');
        $qb->leftJoin('vr.prodFiliales','pf');
        $qb->where('pf is NULL');

        if($ville != null):
            $qb->andWhere('vr.ville = :ville');
            $qb->setParameter("ville", $ville);
        endif;

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return VilleRessources[] Returns an array of VilleRessources objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VilleRessources
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
