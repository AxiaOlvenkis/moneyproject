<?php

namespace App\Repository;

use App\Entity\SocieteFiliales;
use App\Entity\Societes;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SocieteFiliales|null find($id, $lockMode = null, $lockVersion = null)
 * @method SocieteFiliales|null findOneBy(array $criteria, array $orderBy = null)
 * @method SocieteFiliales[]    findAll()
 * @method SocieteFiliales[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SocieteFilialesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SocieteFiliales::class);
    }

    /**
     * @param ArrayCollection $ressource
     * @param Societes $societe
     * @param Ville $ville
     * @return mixed
     */
    public function filialeFromSocieteAndRessources($ressource, $societe, $ville){
        $qb = $this->_em->createQueryBuilder();

        $filiale = $qb->select('f')
            ->from($this->_entityName, 'f')
            ->innerJoin('f.activeProduction','ap')
            ->where('f.societe = :societe')
            ->andWhere('f.ville = :ville')
            ->andWhere('ap.ressourcesSortantes IN (:ressource)')
            ->setParameter("societe", $societe)
            ->setParameter("ville", $ville)
            ->setParameter("ressource", $ressource)
            ->getQuery()->getResult();

        return $filiale;
    }

    // /**
    //  * @return SocieteFiliales[] Returns an array of SocieteFiliales objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SocieteFiliales
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
