<?php

namespace App\Repository;

use App\Entity\MarketFiliales;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MarketFiliales|null find($id, $lockMode = null, $lockVersion = null)
 * @method MarketFiliales|null findOneBy(array $criteria, array $orderBy = null)
 * @method MarketFiliales[]    findAll()
 * @method MarketFiliales[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MarketFilialesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MarketFiliales::class);
    }

    // /**
    //  * @return MarketFiliales[] Returns an array of MarketFiliales objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MarketFiliales
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
