<?php

namespace App\Repository;

use App\Entity\TemporaryVilles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TemporaryVilles|null find($id, $lockMode = null, $lockVersion = null)
 * @method TemporaryVilles|null findOneBy(array $criteria, array $orderBy = null)
 * @method TemporaryVilles[]    findAll()
 * @method TemporaryVilles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TemporaryVillesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TemporaryVilles::class);
    }

    // /**
    //  * @return TemporaryVilles[] Returns an array of TemporaryVilles objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TemporaryVilles
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
