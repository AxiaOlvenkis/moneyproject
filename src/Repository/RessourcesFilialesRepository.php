<?php

namespace App\Repository;

use App\Entity\RessourcesFiliales;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RessourcesFiliales|null find($id, $lockMode = null, $lockVersion = null)
 * @method RessourcesFiliales|null findOneBy(array $criteria, array $orderBy = null)
 * @method RessourcesFiliales[]    findAll()
 * @method RessourcesFiliales[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RessourcesFilialesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RessourcesFiliales::class);
    }

    // /**
    //  * @return RessourcesFiliales[] Returns an array of RessourcesFiliales objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RessourcesFiliales
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
