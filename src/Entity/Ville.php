<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\Api\VillesFreeController;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"ville"}},
 *     collectionOperations={
 *         "get",
 *         "getFreeRessources"={
 *             "method"="GET",
 *             "path"="/villes/free",
 *             "controller"=VillesFreeController::class,
 *             "defaults"={"_api_receive"=false},
 *             "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "ids",
 *                         "in" = "query",
 *                         "required" = "true",
 *                          "type" : "array",
 *                          "items" : {
 *                              "type" : "integer"
 *                          }
 *                     }
 *                 }
 *             }
 *         }},
 *     itemOperations={"get"}
 *  )
 * @ORM\Entity(repositoryClass="App\Repository\VilleRepository")
 */
class Ville
{
    /**
     * @Groups({"pays","ville"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"pays","ville","societe","vRessources","mRessources","fMarket"})
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @Groups({"pays","ville"})
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $argent;

    /**
     * @Groups({ "pays","ville"})
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $population;

    /**
     * @Groups({"ville","societe","vRessources","mRessources","fMarket"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Region", inversedBy="villes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $region;

    /**
     * @Groups({"pays","ville"})
     * @ORM\OneToMany(targetEntity="App\Entity\VilleRessources", mappedBy="ville", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $villeRessources;

    /**
     * @Groups({"ville"})
     * @ORM\OneToMany(targetEntity="App\Entity\SocieteFiliales", mappedBy="ville", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $societeFiliales;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MarketRessources", mappedBy="ville")
     */
    private $marketRessources;

    public function __construct()
    {
        $this->villeRessources = new ArrayCollection();
        $this->societeFiliales = new ArrayCollection();
        $this->marketRessources = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getArgent(): ?int
    {
        return $this->argent;
    }

    public function setArgent(int $argent): self
    {
        $this->argent = $argent;

        return $this;
    }

    public function getPopulation(): ?int
    {
        return $this->population;
    }

    public function setPopulation(int $population): self
    {
        $this->population = $population;

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return Collection|VilleRessources[]
     */
    public function getVilleRessources(): Collection
    {
        return $this->villeRessources;
    }

    public function addVilleRessource(VilleRessources $villeRessource): self
    {
        if (!$this->villeRessources->contains($villeRessource)) {
            $this->villeRessources[] = $villeRessource;
            $villeRessource->setVille($this);
        }

        return $this;
    }

    public function removeVilleRessource(VilleRessources $villeRessource): self
    {
        if ($this->villeRessources->contains($villeRessource)) {
            $this->villeRessources->removeElement($villeRessource);
            // set the owning side to null (unless already changed)
            if ($villeRessource->getVille() === $this) {
                $villeRessource->setVille(null);
            }
        }

        return $this;
    }

    public function resetRessources(){
        $ressources = $this->getVilleRessources();

        foreach($ressources as $r){
            $this->removeVilleRessource($r);
        }
    }

    public function __toString()
    {
        if(!is_string($this->getNom())) {
            return 'NULL';
        }

        return $this->getNom();
    }

    /**
     * @return Collection|SocieteFiliales[]
     */
    public function getSocieteFiliales(): Collection
    {
        return $this->societeFiliales;
    }

    public function addSocieteFiliale(SocieteFiliales $societeFiliale): self
    {
        if (!$this->societeFiliales->contains($societeFiliale)) {
            $this->societeFiliales[] = $societeFiliale;
            $societeFiliale->setVille($this);
        }

        return $this;
    }

    public function removeSocieteFiliale(SocieteFiliales $societeFiliale): self
    {
        if ($this->societeFiliales->contains($societeFiliale)) {
            $this->societeFiliales->removeElement($societeFiliale);
            // set the owning side to null (unless already changed)
            if ($societeFiliale->getVille() === $this) {
                $societeFiliale->setVille(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MarketRessources[]
     */
    public function getMarketRessources(): Collection
    {
        return $this->marketRessources;
    }

    public function addMarketRessource(MarketRessources $marketRessource): self
    {
        if (!$this->marketRessources->contains($marketRessource)) {
            $this->marketRessources[] = $marketRessource;
            $marketRessource->setVille($this);
        }

        return $this;
    }

    public function removeMarketRessource(MarketRessources $marketRessource): self
    {
        if ($this->marketRessources->contains($marketRessource)) {
            $this->marketRessources->removeElement($marketRessource);
            // set the owning side to null (unless already changed)
            if ($marketRessource->getVille() === $this) {
                $marketRessource->setVille(null);
            }
        }

        return $this;
    }
}
