<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\Api\VillesRessourcesController;
use App\Controller\Api\MarketEmplacementController;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"vRessources"}},
 *     collectionOperations={
 *         "getFree"={
 *             "method"="GET",
 *             "path"="/villes/ressources/free",
 *             "controller"=VillesRessourcesController::class,
 *             "defaults"={"_api_receive"=false},
 *             "swagger_context" = {
 *                  "summary" = "Récupération des ressources disponibles pour les utilisateurs pour une ville et une ressource données",
 *                 "parameters" = {
 *                     {
 *                         "name" = "ids",
 *                         "in" = "query",
 *                         "required" = "true",
 *                          "type" : "array",
 *                          "items" : {
 *                              "type" : "integer"
 *                          }
 *                     },
 *                     {
 *                         "name" = "ville",
 *                         "in" = "query",
 *                         "required" = "true",
 *                          "type" : "integer"
 *                     }
 *                 }
 *             }
 *         },
 *         "marketEmplacement"={
 *             "method"="GET",
 *             "path"="/villes/ressources/market",
 *             "controller"=MarketEmplacementController::class,
 *             "defaults"={"_api_receive"=false},
 *             "swagger_context" = {
 *                 "parameters" = {}
 *             }
 *         }},
 *     itemOperations={
 *         "get"
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\VilleRessourcesRepository")
 */
class VilleRessources
{
    /**
     * @Groups({"ville","vRessources","typesEntreprises","societe"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"vRessources","typesEntreprises"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Ville", inversedBy="villeRessources")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ville;

    /**
     * @Groups({"pays","ville","vRessources","typesEntreprises","societe","fMarket"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Ressource", inversedBy="villeRessources")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ressource;

    /**
     * @Groups({"pays","ville","vRessources","typesEntreprises","societe","fMarket"})
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @Groups({"pays","ville","vRessources","typesEntreprises","societe","fMarket"})
     * @ORM\Column(type="integer")
     */
    private $fertilite;

    /**
     * @Groups({"pays","ville","vRessources","typesEntreprises"})
     * @ORM\OneToOne(targetEntity="App\Entity\RessourcesFiliales", mappedBy="villeRessource", cascade={"persist"})
     */
    private $prodFiliales;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getRessource(): ?Ressource
    {
        return $this->ressource;
    }

    public function setRessource(?Ressource $ressource): self
    {
        $this->ressource = $ressource;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getFertilite(): ?int
    {
        return $this->fertilite;
    }

    public function setFertilite(int $fertilite): self
    {
        $this->fertilite = $fertilite;

        return $this;
    }

    public function getProdFiliales(): ?RessourcesFiliales
    {
        return $this->prodFiliales;
    }
}
