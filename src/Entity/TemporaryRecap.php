<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TemporaryRecapRepository")
 */
class TemporaryRecap
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @ORM\Column(type="float")
     */
    private $prix_unite;

    /**
     * @ORM\Column(type="float")
     */
    private $vente;

    /**
     * @ORM\Column(type="string")
     */
    private $tauxstr;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ville")
     */
    private $ville;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\SocieteFiliales")
     */
    private $filiale;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ressource")
     */
    private $ressource;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getPrixUnite(): ?float
    {
        return $this->prix_unite;
    }

    public function setPrixUnite(float $prix_unite): self
    {
        $this->prix_unite = $prix_unite;

        return $this;
    }

    public function getTauxstr(): ?string
    {
        return $this->tauxstr;
    }

    public function setTauxstr(string $tauxstr): self
    {
        $this->tauxstr = $tauxstr;

        return $this;
    }

    public function getVente(): ?float
    {
        return $this->vente;
    }

    public function setVente(float $vente): self
    {
        $this->vente = $vente;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getFiliale(): ?SocieteFiliales
    {
        return $this->filiale;
    }

    public function setFiliale(?SocieteFiliales $filiale): self
    {
        $this->filiale = $filiale;

        return $this;
    }

    public function getSociete(): ?Societes
    {
        return $this->societe;
    }

    public function setSociete(?Societes $societe): self
    {
        $this->societe = $societe;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRessource(): ?Ressource
    {
        return $this->ressource;
    }

    public function setRessource(?Ressource $ressource): self
    {
        $this->ressource = $ressource;

        return $this;
    }
}
