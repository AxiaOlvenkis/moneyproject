<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RessourcesFilialesRepository")
 */
class RessourcesFiliales
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"pays","vRessources", "ville"})
     * @ORM\ManyToOne(targetEntity="App\Entity\SocieteFiliales", inversedBy="ressourcesFiliales",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $filiale;

    /**
     * @Groups({"vRessources","societe","fMarket"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Ressource")
     */
    private $ressource;

    /**
     * @Groups({"vRessources","societe","fMarket"})
     * @ORM\OneToOne(targetEntity="App\Entity\VilleRessources", inversedBy="prodFiliales")
     */
    private $villeRessource;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFiliale(): ?SocieteFiliales
    {
        return $this->filiale;
    }

    public function setFiliale(?SocieteFiliales $filiale): self
    {
        $this->filiale = $filiale;

        return $this;
    }

    public function getRessource(): ?Ressource
    {
        return $this->ressource;
    }

    public function setRessource(?Ressource $ressource): self
    {
        $this->ressource = $ressource;

        return $this;
    }

    public function getVilleRessource(): ?VilleRessources
    {
        return $this->villeRessource;
    }

    public function setVilleRessource(?VilleRessources $villeRessource): self
    {
        $this->villeRessource = $villeRessource;

        return $this;
    }
}
