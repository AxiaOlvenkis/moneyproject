<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\Api\FilialeCreateController;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"filiale"}},
 *     collectionOperations={"get"},
 *     itemOperations={
 *         "get","patch",
 *         "createFiliale"={
 *             "method"="POST",
 *             "path"="/filiale",
 *             "controller"=FilialeCreateController::class,
 *             "defaults"={"_api_receive"=false},
 *             "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "societe",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     },
 *                     {
 *                         "name" = "filiale",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     },
 *                     {
 *                         "name" = "user",
 *                         "required" = "true",
 *                         "type" = "integer"
 *                     },
 *                     {
 *                         "name" = "ville",
 *                         "required" = "true",
 *                         "type" = "integer"
 *                     },
 *                     {
 *                         "name" = "type",
 *                         "required" = "true",
 *                         "type" = "integer"
 *                     },
 *                     {
 *                         "name" = "ressource",
 *                         "required" = "true",
 *                         "type" = "integer"
 *                     }
 *                 }
 *             }
 *         }}
 *  )
 * @ORM\Entity(repositoryClass="App\Repository\SocieteFilialesRepository")
 */
class SocieteFiliales
{
    /**
     * @Groups({"user","societe","filiale"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"vRessources","filiale","mRessources","fMarket","ville"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Societes", inversedBy="societeFiliales")
     * @ORM\JoinColumn(nullable=false)
     */
    private $societe;

    /**
     * @Groups({"vRessources", "user", "societe","filiale","mRessources","fMarket"})
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @Groups({"societe","filiale","fMarket"})
     * @ORM\Column(type="integer")
     */
    private $argent;

    /**
     * @Groups({"societe","filiale"})
     * @ORM\Column(type="integer")
     */
    private $investissement;

    /**
     * @Groups({"societe","fMarket"})
     * @ORM\ManyToOne(targetEntity="App\Entity\TypesEntreprises", inversedBy="societeFiliales")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @Groups({"societe","filiale","fMarket"})
     * @ORM\Column(type="integer")
     */
    private $niveau;

    /**
     * @Groups({"societe","filiale"})
     * @ORM\Column(type="integer")
     */
    private $production;

    /**
     * @Groups({"societe","fMarket"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Ville", inversedBy="societeFiliales")
     */
    private $ville;

    /**
     * @Groups({"societe","fMarket"})
     * @ORM\OneToMany(targetEntity="App\Entity\RessourcesFiliales", mappedBy="filiale", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $ressourcesFiliales;

    /**
     * @Groups({"societe"})
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeEntrepriseProduction")
     */
    private $activeProduction;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MarketRessources", mappedBy="filiale", orphanRemoval=true)
     */
    private $marketRessources;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\MarketFiliales", mappedBy="filiale", cascade={"persist", "remove"})
     */
    private $marketFiliales;

    public function __construct()
    {
        $this->ressourcesFiliales = new ArrayCollection();
        $this->marketRessources = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSociete(): ?Societes
    {
        return $this->societe;
    }

    public function setSociete(?Societes $societe): self
    {
        $this->societe = $societe;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getArgent(): ?int
    {
        return $this->argent;
    }

    public function setArgent(int $argent): self
    {
        $this->argent = $argent;

        return $this;
    }

    public function getInvestissement(): ?int
    {
        return $this->investissement;
    }

    public function setInvestissement(int $investissement): self
    {
        $this->investissement = $investissement;

        return $this;
    }

    public function getType(): ?TypesEntreprises
    {
        return $this->type;
    }

    public function setType(?TypesEntreprises $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getNiveau(): ?int
    {
        return $this->niveau;
    }

    public function setNiveau(int $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getProduction(): ?int
    {
        return $this->production;
    }

    public function setProduction(int $production): self
    {
        $this->production = $production;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return Collection|RessourcesFiliales[]
     */
    public function getRessourcesFiliales(): Collection
    {
        return $this->ressourcesFiliales;
    }

    public function addRessourcesFiliale(RessourcesFiliales $ressourcesFiliale): self
    {
        if (!$this->ressourcesFiliales->contains($ressourcesFiliale)) {
            $this->ressourcesFiliales[] = $ressourcesFiliale;
            $ressourcesFiliale->setFiliale($this);
        }

        return $this;
    }

    public function removeRessourcesFiliale(RessourcesFiliales $ressourcesFiliale): self
    {
        if ($this->ressourcesFiliales->contains($ressourcesFiliale)) {
            $this->ressourcesFiliales->removeElement($ressourcesFiliale);
            // set the owning side to null (unless already changed)
            if ($ressourcesFiliale->getFiliale() === $this) {
                $ressourcesFiliale->setFiliale(null);
            }
        }

        return $this;
    }

    public function getActiveProduction(): ?TypeEntrepriseProduction
    {
        return $this->activeProduction;
    }

    public function setActiveProduction(?TypeEntrepriseProduction $activeProduction): self
    {
        $this->activeProduction = $activeProduction;

        return $this;
    }

    /**
     * @return Collection|MarketRessources[]
     */
    public function getMarketRessources(): Collection
    {
        return $this->marketRessources;
    }

    public function addMarketRessource(MarketRessources $marketRessource): self
    {
        if (!$this->marketRessources->contains($marketRessource)) {
            $this->marketRessources[] = $marketRessource;
            $marketRessource->setFiliale($this);
        }

        return $this;
    }

    public function removeMarketRessource(MarketRessources $marketRessource): self
    {
        if ($this->marketRessources->contains($marketRessource)) {
            $this->marketRessources->removeElement($marketRessource);
            // set the owning side to null (unless already changed)
            if ($marketRessource->getFiliale() === $this) {
                $marketRessource->setFiliale(null);
            }
        }

        return $this;
    }

    public function getMarketFiliales(): ?MarketFiliales
    {
        return $this->marketFiliales;
    }

    public function setMarketFiliales(MarketFiliales $marketFiliales): self
    {
        $this->marketFiliales = $marketFiliales;

        // set the owning side of the relation if necessary
        if ($marketFiliales->getFiliale() !== $this) {
            $marketFiliales->setFiliale($this);
        }

        return $this;
    }
}
