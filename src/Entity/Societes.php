<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\Api\SocieteCreateController;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"societe"}},
 *     collectionOperations={"get"},
 *     itemOperations={
 *         "get","patch",
 *         "createSociety"={
 *             "method"="POST",
 *             "path"="/societe",
 *             "controller"=SocieteCreateController::class,
 *             "defaults"={"_api_receive"=false},
 *             "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "societe",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     },
 *                     {
 *                         "name" = "filiale",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     },
 *                     {
 *                         "name" = "user",
 *                         "required" = "true",
 *                         "type" = "integer"
 *                     },
 *                     {
 *                         "name" = "ville",
 *                         "required" = "true",
 *                         "type" = "integer"
 *                     },
 *                     {
 *                         "name" = "type",
 *                         "required" = "true",
 *                         "type" = "integer"
 *                     },
 *                     {
 *                         "name" = "ressource",
 *                         "required" = "true",
 *                         "type" = "integer"
 *                     }
 *                 }
 *             }
 *         }}
 *  )
 * @ORM\Entity(repositoryClass="App\Repository\SocietesRepository")
 */
class Societes
{
    /**
     * @Groups({"user", "societe"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"vRessources","fMarket","mRessources","ville"})
     * @ORM\JoinColumn(nullable=true)
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="societes", cascade={"persist"})
     */
    private $user;

    /**
     * @Groups({"user", "societe","mRessources","fMarket"})
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @Groups({"societe"})
     * @ORM\Column(type="integer")
     */
    private $argent;

    /**
     * @Groups({"user", "societe"})
     * @ORM\OneToMany(targetEntity="App\Entity\SocieteFiliales", mappedBy="societe", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $societeFiliales;

    public function __construct()
    {
        $this->societeFiliales = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getArgent(): ?int
    {
        return $this->argent;
    }

    public function setArgent(int $argent): self
    {
        $this->argent = $argent;

        return $this;
    }

    /**
     * @return Collection|SocieteFiliales[]
     */
    public function getSocieteFiliales(): Collection
    {
        return $this->societeFiliales;
    }

    public function addSocieteFiliale(SocieteFiliales $societeFiliale): self
    {
        if (!$this->societeFiliales->contains($societeFiliale)) {
            $this->societeFiliales[] = $societeFiliale;
            $societeFiliale->setSociete($this);
        }

        return $this;
    }

    public function removeSocieteFiliale(SocieteFiliales $societeFiliale): self
    {
        if ($this->societeFiliales->contains($societeFiliale)) {
            $this->societeFiliales->removeElement($societeFiliale);
            // set the owning side to null (unless already changed)
            if ($societeFiliale->getSociete() === $this) {
                $societeFiliale->setSociete(null);
            }
        }

        return $this;
    }
}
