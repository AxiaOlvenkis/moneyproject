<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"fMarket"}},
 *     collectionOperations={"get", "post"},
 *     itemOperations={"get"}
 *  )
 * @ORM\Entity(repositoryClass="App\Repository\MarketFilialesRepository")
 */
class MarketFiliales
{
    /**
     * @Groups({"fMarket"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"fMarket"})
     * @ORM\Column(type="float")
     */
    private $prix;

    /**
     * @Groups({"fMarket"})
     * @ORM\OneToOne(targetEntity="App\Entity\SocieteFiliales", inversedBy="marketFiliales", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $filiale;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getFiliale(): ?SocieteFiliales
    {
        return $this->filiale;
    }

    public function setFiliale(SocieteFiliales $filiale): self
    {
        $this->filiale = $filiale;

        return $this;
    }
}
