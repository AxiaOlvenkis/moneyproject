<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TodoListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"todo"}},
 *     collectionOperations={
 *          "get" = {"path"="/noauth/todo"}
 *      },
 *     itemOperations={
 *          "get" = {"path"="/noauth/todo/{id}"}
 *     }
 * )
 * @ApiFilter(OrderFilter::class, properties={"identity":"ASC"})
 * @ORM\Entity(repositoryClass=TodoListRepository::class)
 */
class TodoList
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"todo"})
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @Groups({"todo"})
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @Groups({"todo"})
     * @ORM\Column(type="integer")
     */
    private $priority;

    /**
     * @Groups({"todo"})
     * @ORM\ManyToMany(targetEntity=TagTodo::class, inversedBy="todoLists")
     */
    private $tags;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return Collection|TagTodo[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(TagTodo $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(TagTodo $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }
}
