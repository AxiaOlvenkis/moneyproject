<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Controller\Api\PageController;

/**
 * @ApiResource(
 *     collectionOperations={"get" = {"path"="/noauth/pages"} },
 *     itemOperations={
 *         "get" = {"path"="/noauth/pages/{id}"},
 *         "getBySlug"={
 *             "method"="GET",
 *             "path"="/noauth/pages/slug/{slug}",
 *             "controller"=PageController::class,
 *             "defaults"={"_api_receive"=false},
 *             "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "slug",
 *                         "in" = "path",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     }
 *                 }
 *             }
 *         }
 *     }
 *  )
 * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
 */
class Page
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $contenu;

    /**
     * @Gedmo\Slug(fields={"id", "title"}, updatable=false)
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
