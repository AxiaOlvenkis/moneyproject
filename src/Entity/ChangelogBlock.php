<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChangelogBlockRepository")
 */
class ChangelogBlock
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"changelog"})
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @Groups({"changelog"})
     * @ORM\Column(type="text")
     */
    private $texte;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Changelog", inversedBy="changelogBlocks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $changelog;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getTexte(): ?string
    {
        return $this->texte;
    }

    public function setTexte(string $texte): self
    {
        $this->texte = $texte;

        return $this;
    }

    public function getChangelog(): ?Changelog
    {
        return $this->changelog;
    }

    public function setChangelog(?Changelog $changelog): self
    {
        $this->changelog = $changelog;

        return $this;
    }
}
