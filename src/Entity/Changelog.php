<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"changelog"}},
 *     collectionOperations={
 *          "get" = {"path"="/noauth/changelogs"}
 *      },
 *     itemOperations={
 *          "get" = {"path"="/noauth/changelogs/{id}"}
 *     }
 * )
 * @ApiFilter(OrderFilter::class, properties={"published_at":"DESC"})
 * @ORM\Entity(repositoryClass="App\Repository\ChangelogRepository")
 */
class Changelog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"changelog"})
     * @ORM\Column(type="datetime")
     */
    private $published_at;

    /**
     * @Groups({"changelog"})
     * @ORM\Column(type="string", length=255)
     */
    private $intitule;

    /**
     * @Groups({"changelog"})
     * @ORM\OneToMany(targetEntity="App\Entity\ChangelogBlock", mappedBy="changelog", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $changelogBlocks;

    public function __construct()
    {
        $this->changelogBlocks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->published_at;
    }

    public function setPublishedAt(\DateTimeInterface $published_at): self
    {
        $this->published_at = $published_at;

        return $this;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * @return Collection|ChangelogBlock[]
     */
    public function getChangelogBlocks(): Collection
    {
        return $this->changelogBlocks;
    }

    public function addChangelogBlock(ChangelogBlock $changelogBlock): self
    {
        if (!$this->changelogBlocks->contains($changelogBlock)) {
            $this->changelogBlocks[] = $changelogBlock;
            $changelogBlock->setChangelog($this);
        }

        return $this;
    }

    public function removeChangelogBlock(ChangelogBlock $changelogBlock): self
    {
        if ($this->changelogBlocks->contains($changelogBlock)) {
            $this->changelogBlocks->removeElement($changelogBlock);
            // set the owning side to null (unless already changed)
            if ($changelogBlock->getChangelog() === $this) {
                $changelogBlock->setChangelog(null);
            }
        }

        return $this;
    }
}
