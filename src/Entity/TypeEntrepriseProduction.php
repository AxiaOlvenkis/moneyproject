<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeEntrepriseProductionRepository")
 */
class TypeEntrepriseProduction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypesEntreprises", inversedBy="productions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typesEntreprises;

    /**
     * @Groups({"typesEntreprises","societe"})
     * @ORM\ManyToMany(targetEntity="App\Entity\Ressource", inversedBy="typeEntrepriseProductionsEntrants")
     */
    private $ressourcesEntrantes;

    /**
     * @Groups({"typesEntreprises","societe"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Ressource", inversedBy="typeEntrepriseProductionSortante")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ressourcesSortantes;

    public function __construct()
    {
        $this->ressourcesEntrantes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypesEntreprises(): ?TypesEntreprises
    {
        return $this->typesEntreprises;
    }

    public function setTypesEntreprises(?TypesEntreprises $typesEntreprises): self
    {
        $this->typesEntreprises = $typesEntreprises;

        return $this;
    }

    /**
     * @return Collection|Ressource[]
     */
    public function getRessourcesEntrantes(): Collection
    {
        return $this->ressourcesEntrantes;
    }

    public function addRessourcesEntrante(Ressource $ressourcesEntrante): self
    {
        if (!$this->ressourcesEntrantes->contains($ressourcesEntrante)) {
            $this->ressourcesEntrantes[] = $ressourcesEntrante;
        }

        return $this;
    }

    public function removeRessourcesEntrante(Ressource $ressourcesEntrante): self
    {
        if ($this->ressourcesEntrantes->contains($ressourcesEntrante)) {
            $this->ressourcesEntrantes->removeElement($ressourcesEntrante);
        }

        return $this;
    }

    public function getRessourcesSortantes(): ?Ressource
    {
        return $this->ressourcesSortantes;
    }

    public function setRessourcesSortantes(?Ressource $ressourcesSortantes): self
    {
        $this->ressourcesSortantes = $ressourcesSortantes;

        return $this;
    }

    public function __toString()
    {
        $entrants = $this->getRessourcesEntrantes();
        $sortants = $this->getRessourcesSortantes();

        $str_ar = array();
        foreach ($entrants as $r):
            $str_ar[] = $r->getNom();
        endforeach;

        $str = implode(', ',$str_ar);
        $str .= ' => ' . $sortants->getNom();

        return $str;
    }
}
