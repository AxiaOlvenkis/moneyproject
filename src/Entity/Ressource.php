<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"ressources"}},
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 *  )
 * @ORM\Entity(repositoryClass="App\Repository\RessourceRepository")
 */
class Ressource
{
    /**
     * @Groups({"pays","ville","ressources","typesEntreprises","societe"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"pays","ville","ressources","typesEntreprises","societe","vRessources","mRessources","fMarket"})
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @Groups({"pays","ville","ressources","typesEntreprises","societe","vRessources","fMarket"})
     * @ORM\Column(type="integer")
     */
    private $ratio;

    /**
     * @Groups({"pays","ville","ressources","typesEntreprises","societe","vRessources","fMarket"})
     * @ORM\Column(type="boolean")
     */
    private $epuisable;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Ressource", inversedBy="enfants")
     */
    private $parents;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Ressource", mappedBy="parents")
     */
    private $enfants;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VilleRessources", mappedBy="ressource")
     */
    private $villeRessources;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TypeEntrepriseProduction", mappedBy="ressourcesEntrantes", cascade={"remove"})
     */
    private $typeEntrepriseProductionsEntrants;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TypeEntrepriseProduction", mappedBy="ressourcesSortantes", cascade={"remove"})
     */
    private $typeEntrepriseProductionSortante;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MarketRessources", mappedBy="ressource", orphanRemoval=true)
     */
    private $marketRessources;

    public function __construct()
    {
        $this->parents = new ArrayCollection();
        $this->ressources = new ArrayCollection();
        $this->villeRessources = new ArrayCollection();
        $this->typeEntrepriseProductionsEntrants = new ArrayCollection();
        $this->typeEntrepriseProductionSortante = new ArrayCollection();
        $this->marketRessources = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getRatio(): ?int
    {
        return $this->ratio;
    }

    public function setRatio(int $ratio): self
    {
        $this->ratio = $ratio;

        return $this;
    }

    public function getEpuisable(): ?bool
    {
        return $this->epuisable;
    }

    public function setEpuisable(bool $epuisable): self
    {
        $this->epuisable = $epuisable;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getParents(): Collection
    {
        return $this->parents;
    }

    public function addParent(self $parent): self
    {
        if (!$this->parents->contains($parent)) {
            $this->parents[] = $parent;
        }

        return $this;
    }

    public function removeParent(self $parent): self
    {
        if ($this->parents->contains($parent)) {
            $this->parents->removeElement($parent);
        }

        return $this;
    }

    public function resetParents(){
        $parents = $this->getParents();

        foreach($parents as $p){
            $this->removeParent($p);
        }
    }

    /**
     * @return Collection|self[]
     */
    public function getEnfants(): Collection
    {
        return $this->enfants;
    }

    public function addEnfants(self $enfants): self
    {
        if (!$this->enfants->contains($enfants)) {
            $this->enfants[] = $enfants;
            $enfants->addParent($this);
        }

        return $this;
    }

    public function removeEnfants(self $enfants): self
    {
        if ($this->enfants->contains($enfants)) {
            $this->enfants->removeElement($enfants);
            $enfants->removeParent($this);
        }

        return $this;
    }

    public function __toString()
    {
        if(!is_string($this->getNom())) {
            return 'NULL';
        }

        return $this->getNom();
    }

    /**
     * @return Collection|VilleRessources[]
     */
    public function getVilleRessources(): Collection
    {
        return $this->villeRessources;
    }

    public function addVilleRessource(VilleRessources $villeRessource): self
    {
        if (!$this->villeRessources->contains($villeRessource)) {
            $this->villeRessources[] = $villeRessource;
            $villeRessource->setRessource($this);
        }

        return $this;
    }

    public function removeVilleRessource(VilleRessources $villeRessource): self
    {
        if ($this->villeRessources->contains($villeRessource)) {
            $this->villeRessources->removeElement($villeRessource);
            // set the owning side to null (unless already changed)
            if ($villeRessource->getRessource() === $this) {
                $villeRessource->setRessource(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TypeEntrepriseProduction[]
     */
    public function getTypeEntrepriseProductionsEntrants(): Collection
    {
        return $this->typeEntrepriseProductionsEntrants;
    }

    public function addTypeEntrepriseProductionsEntrant(TypeEntrepriseProduction $typeEntrepriseProductionsEntrant): self
    {
        if (!$this->typeEntrepriseProductionsEntrants->contains($typeEntrepriseProductionsEntrant)) {
            $this->typeEntrepriseProductionsEntrants[] = $typeEntrepriseProductionsEntrant;
            $typeEntrepriseProductionsEntrant->addRessourcesEntrante($this);
        }

        return $this;
    }

    public function removeTypeEntrepriseProductionsEntrant(TypeEntrepriseProduction $typeEntrepriseProductionsEntrant): self
    {
        if ($this->typeEntrepriseProductionsEntrants->contains($typeEntrepriseProductionsEntrant)) {
            $this->typeEntrepriseProductionsEntrants->removeElement($typeEntrepriseProductionsEntrant);
            $typeEntrepriseProductionsEntrant->removeRessourcesEntrante($this);
        }

        return $this;
    }

    /**
     * @return Collection|TypeEntrepriseProduction[]
     */
    public function getTypeEntrepriseProductionSortante(): Collection
    {
        return $this->typeEntrepriseProductionSortante;
    }

    public function addTypeEntrepriseProductionSortante(TypeEntrepriseProduction $typeEntrepriseProductionSortante): self
    {
        if (!$this->typeEntrepriseProductionSortante->contains($typeEntrepriseProductionSortante)) {
            $this->typeEntrepriseProductionSortante[] = $typeEntrepriseProductionSortante;
            $typeEntrepriseProductionSortante->setRessourcesSortantes($this);
        }

        return $this;
    }

    public function removeTypeEntrepriseProductionSortante(TypeEntrepriseProduction $typeEntrepriseProductionSortante): self
    {
        if ($this->typeEntrepriseProductionSortante->contains($typeEntrepriseProductionSortante)) {
            $this->typeEntrepriseProductionSortante->removeElement($typeEntrepriseProductionSortante);
            // set the owning side to null (unless already changed)
            if ($typeEntrepriseProductionSortante->getRessourcesSortantes() === $this) {
                $typeEntrepriseProductionSortante->setRessourcesSortantes(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MarketRessources[]
     */
    public function getMarketRessources(): Collection
    {
        return $this->marketRessources;
    }

    public function addMarketRessource(MarketRessources $marketRessource): self
    {
        if (!$this->marketRessources->contains($marketRessource)) {
            $this->marketRessources[] = $marketRessource;
            $marketRessource->setRessource($this);
        }

        return $this;
    }

    public function removeMarketRessource(MarketRessources $marketRessource): self
    {
        if ($this->marketRessources->contains($marketRessource)) {
            $this->marketRessources->removeElement($marketRessource);
            // set the owning side to null (unless already changed)
            if ($marketRessource->getRessource() === $this) {
                $marketRessource->setRessource(null);
            }
        }

        return $this;
    }
}
