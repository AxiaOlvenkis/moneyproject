<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\Api\MarketRessourcesController;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"mRessources"}},
 *     collectionOperations={
 *          "post",
 *         "marketRessources"={
 *             "method"="GET",
 *             "path"="/ressources/market",
 *             "controller"=MarketRessourcesController::class,
 *             "defaults"={"_api_receive"=false},
 *             "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "ressource",
 *                         "in" = "query",
 *                         "required" = "false",
 *                         "type" : "integer",
 *                     },
 *                     {
 *                         "name" = "ville",
 *                         "in" = "query",
 *                         "required" = "false",
 *                         "type" : "integer",
 *                     }
 *                 }
 *             }
 *         }},
 *     itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\MarketRessourcesRepository")
 */
class MarketRessources
{
    /**
     * @Groups({"mRessources"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"mRessources"})
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @Groups({"mRessources"})
     * @ORM\Column(type="float")
     */
    private $prixUnitaire;

    /**
     * @Groups({"mRessources"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Ressource", inversedBy="marketRessources")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ressource;

    /**
     * @Groups({"mRessources"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Ville", inversedBy="marketRessources")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ville;

    /**
     * @Groups({"mRessources"})
     * @ORM\ManyToOne(targetEntity="App\Entity\SocieteFiliales", inversedBy="marketRessources")
     * @ORM\JoinColumn(nullable=false)
     */
    private $filiale;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getRessource(): ?Ressource
    {
        return $this->ressource;
    }

    public function setRessource(?Ressource $ressource): self
    {
        $this->ressource = $ressource;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getFiliale(): ?SocieteFiliales
    {
        return $this->filiale;
    }

    public function setFiliale(?SocieteFiliales $filiale): self
    {
        $this->filiale = $filiale;

        return $this;
    }

    public function getPrixUnitaire(): ?float
    {
        return $this->prixUnitaire;
    }

    public function setPrixUnitaire(float $prixUnitaire): self
    {
        $this->prixUnitaire = $prixUnitaire;

        return $this;
    }
}
