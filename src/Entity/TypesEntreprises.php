<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"typesEntreprises"}},
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 *  )
 * @ORM\Entity(repositoryClass="App\Repository\TypesEntreprisesRepository")
 */
class TypesEntreprises
{
    /**
     * @Groups({"typesEntreprises","societe"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"typesEntreprises","societe","fMarket"})
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @Groups({"typesEntreprises","societe","fMarket"})
     * @ORM\Column(type="integer")
     */
    private $ratioProduction;

    /**
     * @Groups({"typesEntreprises"})
     * @ORM\OneToMany(targetEntity="App\Entity\TypeEntrepriseProduction", mappedBy="typesEntreprises", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $productions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SocieteFiliales", mappedBy="type")
     */
    private $societeFiliales;

    public function __construct()
    {
        $this->productions = new ArrayCollection();
        $this->societeFiliales = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getRatioProduction(): ?int
    {
        return $this->ratioProduction;
    }

    public function setRatioProduction(int $ratio_production): self
    {
        $this->ratioProduction = $ratio_production;

        return $this;
    }

    /**
     * @return Collection|TypeEntrepriseProduction[]
     */
    public function getProductions(): Collection
    {
        return $this->productions;
    }

    public function addProduction(TypeEntrepriseProduction $production): self
    {
        if (!$this->productions->contains($production)) {
            $this->productions[] = $production;
            $production->setTypesEntreprises($this);
        }

        return $this;
    }

    public function removeProduction(TypeEntrepriseProduction $production): self
    {
        if ($this->productions->contains($production)) {
            $this->productions->removeElement($production);
            // set the owning side to null (unless already changed)
            if ($production->getTypesEntreprises() === $this) {
                $production->setTypesEntreprises(null);
            }
        }

        return $this;
    }

    public function resetProductions(){
        $productions = $this->getProductions();

        foreach($productions as $p){
            $this->removeProduction($p);
        }
    }

    public function __toString()
    {
        if(!is_string($this->getNom())) {
            return 'NULL';
        }

        return $this->getNom();
    }

    /**
     * @return Collection|SocieteFiliales[]
     */
    public function getSocieteFiliales(): Collection
    {
        return $this->societeFiliales;
    }

    public function addSocieteFiliale(SocieteFiliales $societeFiliale): self
    {
        if (!$this->societeFiliales->contains($societeFiliale)) {
            $this->societeFiliales[] = $societeFiliale;
            $societeFiliale->setType($this);
        }

        return $this;
    }

    public function removeSocieteFiliale(SocieteFiliales $societeFiliale): self
    {
        if ($this->societeFiliales->contains($societeFiliale)) {
            $this->societeFiliales->removeElement($societeFiliale);
            // set the owning side to null (unless already changed)
            if ($societeFiliale->getType() === $this) {
                $societeFiliale->setType(null);
            }
        }

        return $this;
    }
}
