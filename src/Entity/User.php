<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Controller\Api\AuthController;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"user"}},
 *     collectionOperations={"get"},
 *     itemOperations={
 *         "get", "put", "delete", "patch",
 *         "register"={
 *             "method"="POST",
 *             "path"="/noauth/user/register",
 *             "controller"=AuthController::class,
 *             "defaults"={"_api_receive"=false},
 *             "swagger_context" = {
 *                 "parameters" = {}
 *             }
 *         }
 *     }
 *
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="site_user")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @Groups({"user"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"user"})
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @Groups({"user"})
     * @ORM\Column(type="string")
     */
    private $full_name;

    /**
     * @var string
     *
     * @Groups({"user","fMarket","mRessources",})
     * @ORM\Column(type="string", unique=true)
     */
    private $username;

    /**
     * A non-persisted field that's used to create the encoded password.
     *
     * @var string
     */
    private $plainPassword;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @Groups({"user"})
     * @ORM\Column(type="text", length=65535)
     */
    private $roles;

    /**
     * @Groups({"user"})
     * @ORM\OneToOne(targetEntity="App\Entity\Societes", mappedBy="user", cascade={"persist", "remove"})
     */
    private $societes;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->full_name;
    }

    /**
     * @param string $full_name
     */
    public function setFullName(string $full_name): void
    {
        $this->full_name = $full_name;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     */
    public function setPlainPassword(string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
        // forces the object to look "dirty" to Doctrine. Avoids
        // Doctrine *not* saving this entity, if only plainPassword changes
        $this->password = null;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param $password
     */
    public function setPassword($password = null): void
    {
        if($password != null && $password != ""):
            $this->password = $password;
        else:
            $this->password = "";
        endif;
    }

    /**
     * Retourne les rôles de l'user
     */
    public function getRoles(): array
    {
        $roles = json_decode($this->roles);

        // Afin d'être sûr qu'un user a toujours au moins 1 rôle
        if (empty($roles)) {
            $roles[] = 'ROLE_USER';
        }

        return array_unique($roles);
    }

    public function hasRole($role){
        $array = $this->getRoles();

        foreach ($array as $a){
            if($a == $role){
                return true;
            }
        }

        return false;
    }

    public function setRoles(array $roles): void
    {
        $this->roles = json_encode($roles);
    }

    public function removeRole(string $role)
    {
        /*if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }
        return $this;*/

        $array = $this->getRoles();
        if (false !== $key = array_search(strtoupper($role), $array, true)) {
            unset($array[$key]);
            $this->setRoles($array);
        }
        return $this;
    }

    public function resetRoles(){
        $roles = $this->getRoles();

        foreach($roles as $r){
            $this->removeRole($r);
        }
    }

    /**
     * Retour le salt qui a servi à coder le mot de passe
     *
     * {@inheritdoc}
     */
    public function getSalt(): ?string
    {
        // See "Do you need to use a Salt?" at https://symfony.com/doc/current/cookbook/security/entity_provider.html
        // we're using bcrypt in security.yml to encode the password, so
        // the salt value is built-in and you don't have to generate one

        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * {@inheritdoc}
     */
    public function eraseCredentials(): void
    {
        $this->plainPassword = null;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize(): string
    {
        return serialize([$this->id, $this->username, $this->password]);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized): void
    {
        [$this->id, $this->username, $this->password] = unserialize($serialized, ['allowed_classes' => false]);
    }

    public function __toString()
    {
        if(!is_string($this->getUsername())) {
            return 'NULL';
        }

        return $this->getUsername();
    }

    public function getSocietes(): ?Societes
    {
        return $this->societes;
    }

    public function setSocietes(?Societes $societes): self
    {
        $this->societes = $societes;

        // set (or unset) the owning side of the relation if necessary
        $newUser = $societes === null ? null : $this;
        if ($newUser !== $societes->getUser()) {
            $societes->setUser($newUser);
        }

        return $this;
    }
}
