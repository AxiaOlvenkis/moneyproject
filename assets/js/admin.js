// Handling the modal confirmation message.
$(document).on('submit', 'form[data-confirmation]', function (event) {
    const $form = $(this);
    const item = $form.find('input[name="item"]').val();
    const $confirm = $('#confirmationModal-' + item);

    if ($confirm.data('result') !== 'yes') {
        //cancel submit event
        event.preventDefault();

        $confirm
            .off('click', '#btnYes')
            .on('click', '#btnYes', function () {
                $confirm.data('result', 'yes');
                $form.find('input[type="submit"]').attr('disabled', 'disabled');
                $form.submit();
            })
            .modal('show');
    }
});

/** ADD LINE (new) **/
let $collectionHolder;
const $addButton = $('<i class="fas fa-plus-circle addline"></i>');
let index = 0;

jQuery(document).ready(function() {
    $collectionHolder = $('tr.toDuplicate');
    index = $collectionHolder.parent('tbody').data('index');
    $('tr.toDuplicate .actionsbutton').append($addButton);

    $('.actionsbutton .addline').on('click', function(e) {
        e.stopPropagation();
        addTableForm($collectionHolder);
        index += 1;
    });

    $collectionHolder.each(function() {
        addTableFormDelete($(this));
    });
});

function addTableForm($collectionHolder) {
    let newForm = $collectionHolder.parent('tbody').data('prototype');
    newForm = newForm.replace(/__name__/g, index);

    const $newFormLi = $('<tr></tr>').append(newForm);
    $newFormLi.children('.actionsbutton').append('<i class="fas fa-plus-circle addline index' + index + '"></i>');

    $('tbody.toDuplicate').append($newFormLi);

    $('.actionsbutton .addline.index' + index).on('click', function(e) {
        e.stopPropagation();
        addTableForm($collectionHolder);
        index += 1;
    });
    addTableFormDelete($newFormLi);
}

function addTableFormDelete($formLine) {
    const $removeFormButton = $('<i class="text-danger fa fa-trash removeLine" aria-hidden="true"></i>');
    $formLine.children('.actionsbutton').append($removeFormButton);

    $removeFormButton.on('click', function(e) {
        $formLine.remove();
        index -= 1;
    });
}